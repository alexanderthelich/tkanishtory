function turnCaret(elem) {
    var $button = $(elem);
    var $caret = $button.children('.caret');
    $caret.removeClass('paused');
    $caret.toggleClass('caret-active');
}

$('#menu-left button, #mobile-catalog button, #mobile-index-menu button').click(function() {
    turnCaret(this);
})

$('#menu-left .caret, #mobile-catalog .caret, #mobile-index-menu .caret').on('animationiteration', function() {
    $(this).addClass('paused');
})

$('#menu-left .collapse, #menu-top .collapse').on(
    'show.bs.collapse hidden.bs.collapse', function(event) {
    event.stopPropagation();
    $(this).find('.caret').each(function() {
        $caret = $(this);
        if ($caret.hasClass('caret-active')) {
            $caret.removeClass('caret-closed').addClass('caret-opened');
        } else {
            $caret.removeClass('caret-opened').addClass('caret-closed');
        }
    });
})

$(document).ready(function() {
    $parents = $('#menu-left .nav-link.active').parents('#menu-left .nav-item');
    $parents.children('button').each(function() {
        $caret = $(this).children('.caret');
        $caret.addClass('caret-active'); 
        $caret.removeClass('caret-closed').addClass('caret-opened');
    });
    $parents.children('.nav.collapse').addClass('show');
});

$('#pallet-bg').change(function() {
    $('body').css('background-color', this.value);
})

$('#pallet-fg').change(function() {
    $('.pallet-fg').css({'background-color': this.value, 'border-color': this.value});
})

$('#pallet-font').change(function() {
    $('.pallet-font').removeClass('text-menu').css('color', this.value);
})
