function makeEmptyImages(imageList) {
    var sample = $('#image-gallery img.sample-image');
    var wrapper = $('#image-gallery .slide-wrapper');
    var originalIds = Object.keys(imageList);
    for (i=0; i<originalIds.length; i++) {
        var newImage = sample.clone().attr('data-original-id', originalIds[i]);
        newImage.removeClass('sample-image');
        wrapper.append(newImage);
    }
    sample.remove();
}

var closeIcon = $('#image-gallery .close');

closeIcon.click(function() {
    $('#image-gallery').modal('hide');
});

function showGallery(targetId) {
    var targetSlide = $('#image-gallery img[data-original-id="'+targetId+'"]');
    if (!targetSlide.attr('src')) {
        targetSlide.attr('src', window.wareImages[targetId]);
    }
    $('#image-gallery img[data-original-id]').hide().removeClass('active');
    targetSlide.show().addClass('active');
    closeIcon.fadeIn('fast');
}

$('img[data-target="#image-gallery"]').click(function() {
    var targetId = $(this).attr('data-original-id');
    var wareId = $(this).attr('data-ware-id');
    if (window.wareImages) {
        showGallery(targetId);
    } else {
        size = Math.min($(window).height(), $(window).width());
        $.get('/catalog/ware_images/', {'ware' : wareId, 'size': size},
            function(data) {
            window.wareImages = data['ware_images'];
            makeEmptyImages(window.wareImages);
            showGallery(targetId);
        });
    }

});

function switchActive(active, to_activate) {
    $('#loader').hide();
    window.setTimeout(function(){
        $('#loader').show();
    }, 450);
    closeIcon.hide();
    active.removeClass('active').fadeOut('fast', function() {
        targetId = to_activate.attr('data-original-id');
        to_activate.attr('src', window.wareImages[targetId]).fadeIn(
            'fast').addClass('active');
        closeIcon.fadeIn(300);
    });
}

$('.arrow-left').click(function() {
    var active = $('#image-gallery img.active');
    var to_activate = active.prev('img[data-original-id]');
    if (!to_activate.length) {
        to_activate = active.siblings('img[data-original-id]').last();
    }
    switchActive(active, to_activate);
});

$('.arrow-right').click(function() {
    var active = $('#image-gallery img.active');
    var to_activate = active.next('img[data-original-id]');
    if (!to_activate.length) {
        to_activate = active.siblings('img[data-original-id]').first();
    }
    switchActive(active, to_activate);
});
