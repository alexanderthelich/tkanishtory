from functools import partialmethod
import os
from tempfile import mkstemp
from threading import Thread
from time import sleep
from urllib.error import HTTPError
from urllib.parse import urljoin

from django.conf import settings
from django.core.files.storage import Storage
from django.utils.deconstruct import deconstructible
from django.utils.module_loading import import_string


@deconstructible
class RemoteStorage(Storage):
    def __new__(cls, *args, **kwargs):
        worker = partialmethod(cls._get_meta_option, option='last_modified')
        for method in ('created_time', 'modified_time', 'accessed_time'):
            setattr(cls, method, worker)
            setattr(cls, 'get_'+method, worker)
        return super().__new__(cls)

    def __init__(self, force_names=False):
        self._provider_class = import_string(settings.REMOTE_STORAGE_CLASS)
        self._force_names = force_names
        self._files_meta = {}

    @property
    def _provider(self):
        return self._provider_class(**settings.REMOTE_STORAGE)

    def _ensure_cleanup(self, tmp_file):
        def cleanup_tmp(tmp_file):
            while not tmp_file.closed:
                sleep(0.2)
            os.unlink(tmp_file.name)
        Thread(target=cleanup_tmp, args=(tmp_file,), daemon=True).start()

    def _open(self, name, mode):
        response = self._provider.get(self.url(name))
        _, tmp_name = mkstemp()
        with open(tmp_name, 'wb') as writer:
            writer.write(response.read())
        tmp_file = open(tmp_name, mode=mode)
        self._ensure_cleanup(tmp_file)
        return tmp_file

    def _save(self, name, content):
        full_name = self.url(name)
        background_upload = Thread(target=self._provider.upload,
                                   args=(content, full_name), daemon=True)
        background_upload.start()
        return name

    def get_available_name(self, name, max_length=None):
        if self._force_names:
            return name
        else:
            return super().get_available_name(name, max_length=max_length)

    def delete(self, name):
        full_name = self.url(name)
        Thread(target=self._provider.delete,
               args=(full_name,), daemon=True).start()

    def _get_meta(self, name):
        if not name in self._files_meta:
            self._files_meta[name] = self._provider.get_object_meta(name)
        return self._files_meta[name]

    def _get_meta_option(self, name, option):
        return self._get_meta(self.url(name))[option]

    def exists(self, name):
        try:
            self._get_meta(self.url(name))
        except HTTPError as err:
            if err.code == 404:
                return False
            else:
                raise
        else:
            return True

    def _names_from_tag(self, page, tag_name):
        nodes = page.getElementsByTagName(tag_name)
        text_nodes = [x.firstChild.data for x in nodes]
        return [os.path.basename(x.strip('/')) for x in text_nodes]

    def listdir(self, path):
        target = self.url(path)
        dir_listing = self._provider.list_objects(target)
        directories, files = [], []
        for page in dir_listing:
            page_prefixes = page.getElementsByTagName('CommonPrefixes')
            for prefix in page_prefixes:
                directories.extend(self._names_from_tag(prefix, 'Prefix'))
            page_files = self._names_from_tag(page, 'Key')
            files.extend(x for x in page_files
                         if not target.rstrip('/').endswith(x))
        return directories, files

    def size(self, name):
        return self._get_meta_option(name, 'size')

    def url(self, name):
        return urljoin(self._provider.base_url, name)
