from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.forms.models import ModelChoiceField, ModelChoiceIterator, ModelForm
from django.forms.widgets import Widget


class TreeModelChoiceIterator(ModelChoiceIterator):
    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label)
        queryset = self.queryset.all()
        for obj in queryset:
            yield self.choice(obj)


class TreeModelChoiceField(ModelChoiceField):
    """A ModelChoiceField with visual improvements for tree sructures."""
    iterator = TreeModelChoiceIterator

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.required = False
        self.null = True
        model = self.queryset.model
        self.queryset = model.hierarchy

    def label_from_instance(self, obj):
        return obj.depth_padded_name


class TreeModelForm(ModelForm):
    """A ModelForm with visual improvements for tree sructures."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sentinel = self.instance._closure_sentinel_attr
        qs = self[self.sentinel].field.queryset
        self[self.sentinel].field.queryset = qs.exclude(
            **{'pk': self.instance.pk})

    def clean(self):
        cleaned_data = super().clean()
        pk = cleaned_data.get('pk')
        parent = cleaned_data.get(self.sentinel)
        if pk is not None and (pk == parent):
            msg = "Element can't refer to itself."
            self.add_error(self.sentinel, msg)
        return cleaned_data

    class Meta:
        field_classes = {
            'parent': TreeModelChoiceField,
        }


class AutoreplacingFormSetMixin:
    """
    Mixin for formsets that enables replacement of some of
    its fields if they do not match specified condition.

    autoreplace_conditions - list of tree-tuples:
     * Name of field to override.
     * Predicate function that can take 2 arguments: child
     value and parent model instance. Child field will use
     replacement value if predicate function returns False.
     * Replacement. Either name of attribute or
     method of parent model instance or an arbitrary
     value. Method is called without any arguments.
     * set_on_instance. If true, new_value will be set
     on form model instance instead of form.data
    """
    autoreplace_conditions = []

    def clean(self, *args, **kwargs):
        self.autoreplace()
        super().clean(*args, **kwargs)

    def autoreplace(self):
        forms = self.forms
        for form, index in zip(forms, range(len(forms))):
            if form.empty_permitted and not form.has_changed():
                continue
            for condition in self.autoreplace_conditions:
                name, predicate, replacement, set_on_instance = condition
                key = form.add_prefix(name)
                if not predicate(form.data.get(key), self.instance):
                    if (isinstance(replacement, str)
                            and hasattr(self.instance, replacement)):
                        new_value = getattr(self.instance, replacement)
                    else:
                        new_value = replacement
                    new_value = new_value() if callable(new_value) else new_value
                    if set_on_instance:
                        setattr(form.instance, name, new_value)
                    else:
                        form.data[key] = new_value
            form.full_clean()
        self.forms = forms


class ReadOnlyWidget(Widget):
    template_name = 'utils/widgets/read_only_widget.html'


class ReadOnlyField(ReadOnlyPasswordHashField):
    widget = ReadOnlyWidget
