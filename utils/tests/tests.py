from django.test import SimpleTestCase
from django.template import Template, TemplateSyntaxError
from django.template.context import Context


class DummyNode:
    _closure_sentinel_attr = 'parent'
    def __init__(self, name, parent):
        self.pk = name
        self.name = name
        self.parent_id = parent

    def __repr__(self):
        return '<{}>'.format(self.name)


class TreeTagTest(SimpleTestCase):
    def setUp(self):
        self.dummy_tree = [
            DummyNode('colors', None),
            DummyNode('red', 'colors'),
            DummyNode('crimson', 'red'),
            DummyNode('scarlet', 'red'),
            DummyNode('blue', 'colors'),
            DummyNode('azure', 'blue'),
            DummyNode('cyan', 'blue'),
            DummyNode('food', None),
            DummyNode('juices', 'food'),
            DummyNode('berries', 'food'),
            DummyNode('strawberry', 'berries'),
            DummyNode('blackberry', 'berries'),
            DummyNode('sweets', 'food'),
        ]
        self.template_outer = ('<ul>{% tree test_tree as branches, node %}'
                               '{}{% endtree %}</ul>')
        self.template_inner = ('<li>{{ node.name }}<ul>{{ branches }}</ul></li>'
                               '{% leaf %}<li>{{ node.name }}</li>')
        self.context = Context()
        self.context['test_tree'] = self.dummy_tree

    def _render_template(self):
        full_template = '{% load project_common %}'
        full_template += self.template_outer.replace('{}', self.template_inner)
        return Template(full_template).render(self.context)

    def test_tree_tag_with_expected_input(self):
        """
        tree tag forms correct structure
        """
        expected_html = """
        <ul>
            <li>colors
                <ul>
                    <li>red
                        <ul>
                            <li>crimson</li>
                            <li>scarlet</li>
                        </ul>
                    </li>
                    <li>blue
                        <ul>
                            <li>azure</li>
                            <li>cyan</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>food
                <ul>
                    <li>juices</li>
                    <li>berries
                        <ul>
                            <li>strawberry</li>
                            <li>blackberry</li>
                        </ul>
                    </li>
                    <li>sweets</li>
                </ul>
            </li>
        </ul>
        """
        self.assertHTMLEqual(expected_html, self._render_template())

    def test_tree_tag_with_empty_tree(self):
        """
        Check behavior when there are no nodes in a tree.
        """
        expected_html = '<ul></ul>'
        self.context = Context()
        self.context['test_tree'] = []
        self.assertHTMLEqual(expected_html, self._render_template())

    def test_tree_tag_without_leaf_tag(self):
        """
        tree tag fails correctly when no leaf tag given
        """
        self.template_inner = '<li>{{ node.name }}<ul>{{ branches }}</li>'
        self.assertRaisesMessage(TemplateSyntaxError, 'Missing leaf tag',
                                 self._render_template)

    def test_tree_tag_with_invalid_syntax(self):
        """
        Correct failure when one of the variables is not specified
        """
        self.template_outer = ('<ul>{% tree %}{}{% endtree %}</ul>')
        self.assertRaisesMessage(
            TemplateSyntaxError, 'tree tag requires arguments',
            self._render_template)
        self.template_outer = ('<ul>{% tree test_tree as branches %}'
                               '{}{% endtree %}</ul>')
        self.assertRaisesMessage(
            TemplateSyntaxError,
            "tree tag should be of format 'tree x as y, z'",
            self._render_template)
        self.template_outer = ('<ul>{% tree test_tree as branches, %}'
                               '{}{% endtree %}</ul>')
        self.assertRaisesMessage(
            TemplateSyntaxError,
            "tree tag should be of format 'tree x as y, z'",
            self._render_template)

    def test_tree_tag_clears_up_its_context_variables(self):
        """
        All context variables, created during rendering are cleared up
        """
        self.template_outer += '<span>{{ node.name }}-{{ branches }}</span>'
        self.assertInHTML('<span>-</span>', self._render_template())
