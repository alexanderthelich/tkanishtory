from unittest import skip, expectedFailure

from django.test import TestCase
from django.db.models import CharField, OuterRef, Subquery
from django.db.models.expressions import Col, F, RawSQL, Value

from ..models import HierarchicalQuerySet
from menus.models import MenuItem
from utils.win_func import DenseRank


class HierarchicalQuerySetTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.primary_menu = 'stuff'
        cls.secondary_menu = 'secondary'
        items_data = [
            {'title': 'Colors', 'visible': True,
             'sort': 2000, 'parent': 'Everything'},
            {'title': 'Red', 'visible': False,
             'sort': 1000, 'parent': 'Colors'},
            {'title': 'Crimson', 'visible': True,
             'sort': 5000, 'parent': 'Red'},
            {'title': 'Scarlet', 'visible': True,
             'sort': 1000, 'parent': 'Red'},
            {'title': 'Magenta', 'visible': True,
             'sort': 4000, 'parent': 'Red'},
            {'title': 'Maroon', 'visible': True,
             'sort': 1000, 'parent': 'Red'},
            {'title': 'Blue', 'visible': True,
             'sort': 3000, 'parent': 'Colors'},
            {'title': 'Azure', 'visible': False,
             'sort': 1000, 'parent': 'Blue'},
            {'title': 'Cyan', 'visible': True,
             'sort': 1000, 'parent': 'Blue'},
            {'title': 'Indigo', 'visible': False,
             'sort': 1000, 'parent': 'Blue'},
            {'title': 'Teal', 'visible': True,
             'sort': 1000, 'parent': 'Blue'},
            {'title': 'Food', 'visible': True,
             'sort': 1000, 'parent': 'Everything'},
            {'title': 'Meat', 'visible': True,
             'sort': 3000, 'parent': 'Food'},
            {'title': 'Pork', 'visible': True,
             'sort': 2000, 'parent': 'Meat'},
            {'title': 'Beef', 'visible': True,
             'sort': 1000, 'parent': 'Meat'},
            {'title': 'Poultry', 'visible': True,
             'sort': 1000, 'parent': 'Meat'},
            {'title': 'Mutton', 'visible': True,
             'sort': 1000, 'parent': 'Meat'},
            {'title': 'Vegetables', 'visible': True,
             'sort': 1000, 'parent': 'Food'},
            {'title': 'Carrot', 'visible': True,
             'sort': 1000, 'parent': 'Vegetables'},
            {'title': 'Lettuse', 'visible': False,
             'sort': 1000, 'parent': 'Vegetables'},
            {'title': 'Potato', 'visible': False,
             'sort': 1000, 'parent': 'Vegetables'},
            {'title': 'Onion', 'visible': True,
             'sort': 1000, 'parent': 'Vegetables'},
        ]
        MenuItem.objects.create(title='Everything', visible=True, parent=None,
                                url='/', menu=cls.primary_menu)
        for item in items_data:
            parent_id = MenuItem.objects.get(title=item['parent'])
            MenuItem.objects.create(
                menu=cls.primary_menu, parent=parent_id, url='/',
                title=item['title'], visible=item['visible'], sort=item['sort']
            )

    def tearDown(self):
        MenuItem.objects.update(menu=self.primary_menu)

    def _assign_to_secondary(self):
        color_item = MenuItem.objects.get(title='Colors')
        qs = MenuItem.objects.filter(
            menuitemclosure_parents__parent_id=color_item)
        qs.update(menu=self.secondary_menu)

    def _check_titles(self, qs, expected_titles):
        titles = [x.title for x in qs]
        self.assertEqual(titles, expected_titles)

    def test_order_by(self):
        expected_titles_asc = [
            'Everything', 'Colors', 'Blue', 'Azure', 'Cyan', 'Indigo', 'Teal',
            'Red', 'Crimson', 'Magenta', 'Maroon', 'Scarlet', 'Food', 'Meat',
            'Beef', 'Mutton', 'Pork', 'Poultry', 'Vegetables', 'Carrot',
            'Lettuse', 'Onion', 'Potato',
        ]
        expected_titles_desc = [
            'Everything', 'Food', 'Vegetables', 'Potato', 'Onion', 'Lettuse',
            'Carrot', 'Meat', 'Poultry', 'Pork', 'Mutton', 'Beef',
            'Colors', 'Red', 'Scarlet', 'Maroon', 'Magenta', 'Crimson',
            'Blue', 'Teal', 'Indigo', 'Cyan', 'Azure',
        ]
        qs_asc = MenuItem.hierarchy.order_by('title')
        qs_desc = MenuItem.hierarchy.order_by('-title')
        self._check_titles(qs_asc, expected_titles_asc)
        self._check_titles(qs_desc, expected_titles_desc)

    def test_default_ordering(self):
        expected_titles = [
            'Everything', 'Colors',
            'Blue', 'Azure', 'Cyan', 'Indigo', 'Teal',
            'Red', 'Crimson', 'Magenta', 'Maroon', 'Scarlet',
            'Food',
            'Meat', 'Pork', 'Beef', 'Mutton', 'Poultry',
            'Vegetables', 'Carrot', 'Lettuse', 'Onion', 'Potato',
        ]
        MenuItem._meta.ordering = ['-sort', 'title']
        qs = MenuItem.hierarchy.all()
        self._check_titles(qs, expected_titles)

    def test_without_any_ordering(self):
        qs = MenuItem.hierarchy.order_by()
        sample = [x for x in qs][0]
        crumbs = [sample.parent_id]
        previous = None
        for i in qs:
            parent = i.parent_id
            # Filtering out sibling order, always even element
            path = i.path[::2]
            if parent == previous and parent != crumbs[-1]:
                crumbs.append(previous)
            else:
                try:
                    while crumbs[-1] != parent:
                        crumbs.pop()
                except IndexError:
                    self.fail('Hierarchy was formed incorectly')
            with self.subTest(node=i):
                self.assertEqual(crumbs, path)
            previous = i.pk

    def test_filter(self):
        self._assign_to_secondary()
        qs = MenuItem.hierarchy.filter(menu=self.secondary_menu)
        qs = qs.order_by('-title')
        expected_titles = [
            'Colors',
            'Red', 'Scarlet', 'Maroon', 'Magenta', 'Crimson',
            'Blue', 'Teal', 'Indigo', 'Cyan', 'Azure',
        ]
        self._check_titles(qs, expected_titles)

    def test_exclude(self):
        self._assign_to_secondary()
        qs = MenuItem.hierarchy.exclude(menu=self.secondary_menu)
        qs = qs.order_by('-title')
        expected_titles = [
            'Everything', 'Food',
            'Vegetables', 'Potato', 'Onion', 'Lettuse', 'Carrot',
            'Meat', 'Poultry', 'Pork', 'Mutton', 'Beef',
        ]
        self._check_titles(qs, expected_titles)

    def test_filter_with_subquery(self):
        self._assign_to_secondary()
        items_secondary = MenuItem.objects.filter(menu=self.secondary_menu)
        items_secondary = items_secondary.values('pk')
        qs = MenuItem.hierarchy.filter(pk__in=Subquery(items_secondary))
        qs = qs.order_by('-title')
        expected_titles = [
            'Colors',
            'Red', 'Scarlet', 'Maroon', 'Magenta', 'Crimson',
            'Blue', 'Teal', 'Indigo', 'Cyan', 'Azure',
        ]
        self._check_titles(qs, expected_titles)

    def test_exclude_with_subquery(self):
        self._assign_to_secondary()
        items_secondary = MenuItem.objects.filter(menu=self.secondary_menu)
        items_secondary = items_secondary.values('pk')
        qs = MenuItem.hierarchy.exclude(pk__in=Subquery(items_secondary))
        qs = qs.order_by('-title')
        expected_titles = [
            'Everything', 'Food',
            'Vegetables', 'Potato', 'Onion', 'Lettuse', 'Carrot',
            'Meat', 'Poultry', 'Pork', 'Mutton', 'Beef',
        ]
        self._check_titles(qs, expected_titles)

    def test_filter_tree_for_all(self):
        expected_titles = [
            'Everything', 'Colors', 'Blue', 'Cyan', 'Teal',
            'Food', 'Meat', 'Beef', 'Mutton', 'Pork', 'Poultry',
            'Vegetables', 'Carrot', 'Onion',
        ]
        qs = MenuItem.hierarchy.visible_items().order_by('title')
        self._check_titles(qs, expected_titles)

    @expectedFailure
    def test_exclude_tree_for_all(self):
        expected_titles = [
            'Azure', 'Indigo', 'Lettuse', 'Potato',
            'Red', 'Crimson', 'Magenta', 'Maroon', 'Scarlet',
        ]
        qs = MenuItem.hierarchy.exclude_tree(visible=True).order_by('title')
        self._check_titles(qs, expected_titles)

    @expectedFailure
    def test_filter_tree_not_for_all(self):
        expected_titles = [
            'Azure', 'Indigo', 'Lettuse', 'Potato',
            'Red', 'Crimson', 'Magenta', 'Maroon', 'Scarlet',
        ]
        qs = MenuItem.hierarchy.filter_tree(visible=False, for_all=False)
        qs = qs.order_by('title')
        self._check_titles(qs, expected_titles)

    def test_exclude_tree_not_for_all(self):
        expected_titles = [
            'Everything', 'Colors', 'Blue', 'Cyan', 'Teal',
            'Food', 'Meat', 'Beef', 'Mutton', 'Pork', 'Poultry',
            'Vegetables', 'Carrot', 'Onion',
        ]
        qs = MenuItem.hierarchy.exclude_tree(visible=False, for_all=False)
        qs = qs.order_by('title')
        self._check_titles(qs, expected_titles)

    def test_add_root(self):
        qs = MenuItem.hierarchy.add_root(title='Colors')
        qs = qs.order_by('-title')
        expected_titles = [
            'Colors',
            'Red', 'Scarlet', 'Maroon', 'Magenta', 'Crimson',
            'Blue', 'Teal', 'Indigo', 'Cyan', 'Azure',
        ]
        self._check_titles(qs, expected_titles)

    def test_tree_filtering_with_set_root(self):
        qs = MenuItem.hierarchy.add_root(title='Colors')
        qs = qs.exclude_tree(title__startswith='M', for_all=False)
        qs = qs.order_by('-title')
        expected_titles = [
            'Colors',
            'Red', 'Scarlet', 'Crimson',
            'Blue', 'Teal', 'Indigo', 'Cyan', 'Azure',
        ]
        self._check_titles(qs, expected_titles)

    def test_annotate(self):
        qs = MenuItem.hierarchy.get_queryset().order_by('title')
        expected_titles = [
            'Everything', 'Colors',
            'Blue', 'Azure', 'Cyan', 'Indigo', 'Teal',
            'Red', 'Crimson', 'Magenta', 'Maroon', 'Scarlet',
            'Food', 'Meat', 'Beef', 'Mutton', 'Pork', 'Poultry',
            'Vegetables', 'Carrot', 'Lettuse', 'Onion', 'Potato',
        ]
        val_qs = qs.annotate(test_col=Value('test', output_field=CharField()))
        col_qs = qs.annotate(test_col=F('sort')*2)
        subq = MenuItem.objects.filter(parent_id=None).values('pk')
        subq_qs = qs.annotate(test_col=Subquery(subq))
        params = [('val_qs', [x.test_col=='test' for x in val_qs], val_qs),
                  ('col_qs', [x.test_col==x.sort*2 for x in col_qs], col_qs),
                  ('subq_qs', [x.test_col==subq[0]['pk'] for x in subq_qs], subq_qs)]
        for k, results, query_set in params:
            with self.subTest(query_set=k):
                self.assertIs(all(results), True)
                self.assertEqual([x.title for x in query_set], expected_titles)

    @skip('to be done later')
    def test_update(self):
        pass

    @skip('to be done later')
    def test_delete(self):
        pass
