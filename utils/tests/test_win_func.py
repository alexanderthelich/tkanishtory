from django.core.exceptions import FieldError
from django.db.models.expressions import Q, Star
from django.db.models.functions.base import Cast, Length
from django.db.models import CharField, IntegerField
from django.db.utils import ProgrammingError
from django.test import TestCase

from misc_pages.models import Page
from ..win_func import ArrayAggMult, DenseRank, WinFunc


class WinFuncTest(TestCase):
    INITIAL_SIZE = 10

    @classmethod
    def setUpTestData(cls):
        Page.objects.bulk_create([
            Page(url='our_staff', name='Our Staff', title='Our Staff', visible=True),
            Page(url='events', name='Events',
                 keywords='event, entertainment, leisure'),
            Page(url='contacts', name='Contacts', title='Contact information',
                 keywords='contacts, address, phone', visible=True),
            Page(url='partners', name='Our partners', title='Partners'),
            Page(url='merchandise', name='Goods for sale', visible=True),
            Page(url='discounts', name='Special offers',
                 keywords='discounts, special offers, sales', visible=True),
            Page(url='faq', name='FAQ', visible=True),
            Page(url='about', name='History of Company'),
            Page(url='blog', name='News & Articles', visible=True),
            Page(url='departments', name='Department Map'),
        ])

    def _make_qs_string(self, partition_by=None, order_by=None):
        qs = Page.objects.annotate(
            amount_by_visible=WinFunc(
                Star(), function='COUNT', partition_by=partition_by,
                order_by=order_by, output_field=IntegerField())
        )
        return qs.query.__str__(), qs

    def test_with_partition_by(self):
        query_string, qs = self._make_qs_string(partition_by='visible')
        over_clause = 'OVER(PARTITION BY "{}"."visible")'.format(Page._meta.db_table)
        self.assertIn(over_clause, query_string)
        self.assertEqual(self.INITIAL_SIZE, len(qs))

    def test_with_order_by(self):
        query_string, qs = self._make_qs_string(order_by='name')
        clause_template = 'OVER(ORDER BY "{}"."name"{} ASC)'
        over_clause = clause_template.format(Page._meta.db_table, '')
        self.assertIn(over_clause, query_string)
        self.assertEqual(self.INITIAL_SIZE, len(qs))

    def test_with_order_by_desc(self):
        clause_template = 'OVER(ORDER BY "{}"."name" DESC)'
        query_string_desc, qs = self._make_qs_string(order_by='-name')
        over_clause_desc = clause_template.format(Page._meta.db_table)
        self.assertIn(over_clause_desc, query_string_desc)

    def test_with_multiple_order_by(self):
        clause_template = 'OVER(ORDER BY "{0}"."name" DESC, "{0}"."url" ASC)'
        query_string_desc, qs = self._make_qs_string(order_by=['-name', 'url'])
        over_clause_desc = clause_template.format(Page._meta.db_table)
        self.assertIn(over_clause_desc, query_string_desc)

    def test_with_empty_order_by(self):
        query_string, qs = self._make_qs_string(
            partition_by='visible', order_by=[])
        try:
            len(qs)
        except ProgrammingError as err:
            self.fail(err)

    def test_with_partition_and_order_by(self):
        query_string, qs = self._make_qs_string(partition_by='visible',
                                                order_by='name')
        over_clause = 'OVER(PARTITION BY "{0}"."visible" ORDER BY "{0}"."name" ASC)'
        over_clause = over_clause.format(Page._meta.db_table)
        self.assertIn(over_clause, query_string)
        self.assertEqual(self.INITIAL_SIZE, len(qs))

    def test_with_complex_arguments_in_over_clause(self):
        query_string, qs = self._make_qs_string(
            partition_by=Q(title__isnull=True), order_by=Length('url'))
        over_clause = ('OVER(PARTITION BY "{0}"."title" IS NULL'
                       ' ORDER BY LENGTH("{0}"."url") ASC)')
        over_clause = over_clause.format(Page._meta.db_table)
        self.assertIn(over_clause, query_string)
        self.assertEqual(self.INITIAL_SIZE, len(qs))

    def test_with_query_wide_order_by(self):
        qs = Page.objects.annotate(
            amount_by_visible=WinFunc(Star(),
                function='COUNT', partition_by='visible', order_by='name',
                output_field=IntegerField())
        ).order_by('url')
        query_string = qs.query.__str__()
        over_clause = ('OVER(PARTITION BY "{0}"."visible"'
                       ' ORDER BY "{0}"."name" ASC)')
        over_clause = over_clause.format(Page._meta.db_table)
        order_by_clause = 'ORDER BY "{}"."url"'.format(Page._meta.db_table)
        self.assertIn(over_clause, query_string)
        self.assertIn(order_by_clause, query_string)
        self.assertEqual(self.INITIAL_SIZE, len(qs))

    def test_multiple_partition_by_not_accepted(self):
        self.assertRaisesRegex(
            TypeError, 'accepts exactly one column', self._make_qs_string,
            partition_by=['visible', 'url'])

class ArrayAggMultTest(TestCase):
    INITIAL_SIZE = 10

    @classmethod
    def setUpTestData(cls):
        Page.objects.bulk_create([
            Page(url='about', name='History of Company'),
            Page(url='blog', name='News & Articles', visible=True),
            Page(url='contacts', name='Contacts', title='Contact information',
                 keywords='contacts, address, phone', visible=True),
            Page(url='departments', name='Department Map'),
            Page(url='discounts', name='Special offers',
                 keywords='discounts, special offers, sales', visible=True),
            Page(url='events', name='Events',
                 keywords='event, entertainment, leisure'),
            Page(url='faq', name='FAQ', visible=True),
            Page(url='merchandise', name='Goods for sale', visible=True),
            Page(url='our_staff', name='Our Staff', title='Our Staff', visible=True),
            Page(url='partners', name='Our partners', title='Partners'),
        ])

    def _make_qs(self, *args, partition_by=None,
                 order_by=None, output_field=None):
        return Page.objects.annotate(
            test_field=ArrayAggMult(
                *args, partition_by=partition_by, order_by=order_by,
                output_field=output_field)
        ).order_by('-visible', 'url')

    def test_with_no_arguments(self):
        self.assertRaisesRegex(TypeError, 'must have at least one argument',
                               self._make_qs, partition_by='visible',
                               order_by='url')

    def test_with_one_argument(self):
        test_qs = self._make_qs('url', partition_by='visible')
        sample_qs = Page.objects.order_by('-visible', 'url')
        visible_urls = []
        invisible_urls = []
        for i in sample_qs:
            if i.visible:
                visible_urls.append(i.url)
            else:
                invisible_urls.append(i.url)
        self.assertEqual(len(sample_qs), len(test_qs))
        self.assertEqual(sorted(test_qs.first().test_field), visible_urls)
        self.assertEqual(sorted(test_qs.last().test_field), invisible_urls)

    def test_with_multiple_arguments(self):
        test_qs = self._make_qs('url', 'name', partition_by='visible',
                                order_by='url')
        sample_qs = Page.objects.order_by('-visible', 'url')
        visible_pairs = []
        invisible_pairs = []
        for i in sample_qs:
            if i.visible:
                pairs_type = visible_pairs
            else:
                pairs_type = invisible_pairs
            pairs_type.extend([i.url, i.name])
        test_last_visible = test_qs[int(len(visible_pairs)/2 - 1)]
        self.assertEqual(len(sample_qs), len(test_qs))
        self.assertEqual(test_last_visible.test_field, visible_pairs)
        self.assertEqual(test_qs.last().test_field, invisible_pairs)

    def test_with_arguments_of_different_types(self):
        char_field = CharField(max_length=200)
        mix_type_qs = self._make_qs(
            'url', 'visible', partition_by='visible', order_by='url')
        test_qs = self._make_qs(
            Cast('url', char_field), Cast('visible', char_field),
            partition_by='visible', order_by='url'
        )
        sample_qs = Page.objects.order_by('-visible', 'url')
        visible_pairs = []
        invisible_pairs = []
        for i in sample_qs:
            if i.visible:
                pairs_type = visible_pairs
            else:
                pairs_type = invisible_pairs
            pairs_type.extend([i.url, str(i.visible).lower()])
        test_last_visible = test_qs[int(len(visible_pairs)/2 - 1)]
        self.assertEqual(len(sample_qs), len(test_qs))
        self.assertEqual(test_last_visible.test_field, visible_pairs)
        self.assertEqual(test_qs.last().test_field, invisible_pairs)
        self.assertRaises(FieldError, len, mix_type_qs)


class DenseRankTest(TestCase):
    INITIAL_SIZE = 10

    @classmethod
    def setUpTestData(cls):
        Page.objects.bulk_create([
            Page(url='about', name='History of Company'),
            Page(url='blog', name='News & Articles', visible=True),
            Page(url='contacts', name='Contacts', title='Contact information',
                 keywords='contacts, address, phone', visible=True),
            Page(url='departments', name='Department Map'),
            Page(url='discounts', name='Special offers',
                 keywords='discounts, special offers, sales', visible=True),
            Page(url='events', name='Events',
                 keywords='event, entertainment, leisure'),
            Page(url='faq', name='FAQ', visible=True),
            Page(url='merchandise', name='Goods for sale', visible=True),
            Page(url='our_staff', name='Our Staff', title='Our Staff', visible=True),
            Page(url='partners', name='Our partners', title='Partners'),
        ])

    def _make_qs(self, *args, partition_by=None,
                 order_by=None, output_field=None):
        return Page.objects.annotate(
            test_field=DenseRank(
                *args, partition_by=partition_by, order_by=order_by,
                output_field=output_field)
        ).order_by('url')

    def test_accepts_no_arguments(self):
        self.assertRaisesRegex(TypeError, 'takes exactly 0 arguments',
                               self._make_qs, 'name', partition_by='visible')

    def test_correct_numbering(self):
        test_qs = self._make_qs(partition_by='visible', order_by='url',
                                output_field=IntegerField())
        visible_urls = Page.objects.order_by('url').exclude(visible=False)
        invisible_urls = Page.objects.order_by('url').exclude(visible=True)
        visible_pairs = list(enumerate([x.url for x in visible_urls], 1))
        invisible_pairs = list(enumerate([x.url for x in invisible_urls], 1))
        test_visible_pairs = []
        test_invisible_pairs = []
        for i in list(test_qs):
            if i.visible:
                pair_type = test_visible_pairs
            else:
                pair_type = test_invisible_pairs
            pair_type.append((i.test_field, i.url))
        self.assertEqual(visible_pairs, test_visible_pairs)
        self.assertEqual(invisible_pairs, test_invisible_pairs)
