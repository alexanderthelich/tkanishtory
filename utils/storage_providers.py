import datetime
from functools import reduce, wraps
import hashlib
import hmac
from mimetypes import guess_type
import urllib.parse as uparse
from urllib.request import Request, urlopen
import xml.dom.minidom as minidom


def sends_request(method):
    @wraps(method)
    def wrapper(*args, **kwargs):
        obj = args[0]
        obj._request = Request(obj._host_name, data=b'')
        obj._now = datetime.datetime.now(tz=datetime.timezone.utc)
        method(*args, **kwargs)
        return obj._send_request()
    return wrapper

class YandexCloud(object):
    _signing_algorithm = 'AWS4-HMAC-SHA256'

    def __init__(self, secret_key_id, secret_key, region, service_name,
                 request_type, host_name, bucket_name, upload_dir):
        self._secret_key_id = secret_key_id
        self._secret_key = secret_key
        self._host_name = host_name
        self._bucket_name = bucket_name
        self._region = region
        self._service_name = service_name
        self._request_type = request_type
        path = '{}/{}/'.format(self._bucket_name, upload_dir)
        self.base_url = uparse.urljoin(self._host_name, path)

    @property
    def _scope(self):
        return [self._now.strftime('%Y%m%d'), self._region,
                self._service_name, self._request_type]

    def _sha256(self, content):
        return hashlib.sha256(content).hexdigest()

    def _hashed_payload(self):
        return self._sha256(self._request.data)

    def _new_hmac(self, key, msg):
        if not isinstance(key, bytes):
            key = key.encode()
        return hmac.new(key, msg=msg.encode(), digestmod=hashlib.sha256)

    def _signing_key(self):
        key_parts = ['AWS4' + self._secret_key] + self._scope
        return reduce(lambda x,y: self._new_hmac(x,y).digest(), key_parts)

    def _canonical_qs(self, query_string):
        query_params = uparse.parse_qsl(query_string, keep_blank_values=True)
        return uparse.urlencode(sorted(query_params, key=lambda x: x[0]))

    def _signable_headers(self):
        headers = ['Content-type', 'Host']
        headers += [x for x in self._request.headers.keys()
                    if x.startswith('X-amz-')]
        return sorted(headers, key=lambda x: x.lower())

    def _canonical_headers(self):
        headers = []
        for header_name in self._signable_headers():
            value = self._request.get_header(header_name).strip()
            headers.append('{}:{}'.format(header_name.lower(), value))
        return '\n'.join(headers)

    def _signed_headers(self):
        return ';'.join(x.lower() for x in self._signable_headers())

    def _canonical_request(self):
        parsed_result = uparse.urlparse(self._request.full_url)
        parts = [
            self._request.method,
            uparse.quote(parsed_result.path) or '/',
            self._canonical_qs(parsed_result.query),
            self._canonical_headers() + '\n',
            self._signed_headers(),
            self._request.get_header('X-amz-content-sha256'),
        ]
        return '\n'.join(parts)

    def _string_to_sign(self):
        parts = [
            self._signing_algorithm,
            self._request.get_header('X-amz-date'),
            '/'.join(self._scope),
            self._sha256(self._canonical_request().encode()),
        ]
        return '\n'.join(parts)

    def _signature(self):
        return self._new_hmac(
            self._signing_key(), self._string_to_sign()).hexdigest()

    def _add_authorization(self):
        parts = [
            ('Credential', '/'.join([self._secret_key_id] + self._scope)),
            ('SignedHeaders', self._signed_headers()),
            ('Signature', self._signature()),
        ]
        auth_info = ','.join(f'{k}={v}' for k, v in parts)
        auth_full = self._signing_algorithm + ' ' + auth_info
        self._request.add_header('Authorization', auth_full)

    def _add_common_headers(self):
        headers = {
            'Host': self._request.host,
            'X-amz-date': self._now.strftime('%Y%m%dT%H%M%SZ'),
            'X-amz-content-sha256': self._hashed_payload(),
        }
        for k, v in headers.items():
            self._request.add_header(k, v)

    def _send_request(self):
        self._add_common_headers()
        self._add_authorization()
        return urlopen(self._request)

    @sends_request
    def list_buckets(self):
        self._request.method = 'GET'
        self._request.full_url = uparse.urljoin(self._host_name, '')
        self._request.add_header('Content-type', 'message/http')

    @sends_request
    def upload(self, source, dest, storage_class=None):
        self._request.method = 'PUT'
        self._request.full_url = dest
        self._request.data = source.read()
        mime_type, _ = guess_type(dest)
        self._request.add_header('Content-type', mime_type)
        if storage_class is not None:
            self._request.add_header('X-amz-storage-class', storage_class)

    @sends_request
    def delete(self, path):
        self._request.method = 'DELETE'
        self._request.full_url = path
        self._request.add_header('Content-type', 'message/http')

    @sends_request
    def get(self, path):
        self._request.method = 'GET'
        self._request.full_url = path
        self._request.add_header('Content-type', 'message/http')

    def download(self, source, dest):
        response = self.get(source)
        with open(dest, 'wb') as fh:
            fh.write(response.read())

    @sends_request
    def _get_object_meta(self, path):
        self._request.method = 'HEAD'
        self._request.full_url = path
        self._request.add_header('Content-type', 'message/http')

    def get_object_meta(self, path):
        response = self._get_object_meta(path)
        return {
            'size': response.getheader('Content-Length'),
            'last_modified': response.getheader('Last-Modified'),
        }

    @sends_request
    def _list_objects(self, path, continuation_token=None):
        self._request.method = 'GET'
        base_path = uparse.urljoin(self._host_name, self._bucket_name)
        _, prefix = path.split(base_path)
        params = {
            'list-type': 2,
            'delimiter': '/',
            'prefix': prefix.lstrip('/'),
            'max-keys': 2,
        }
        if continuation_token is not None:
            params['continuation-token'] = continuation_token
        query_string = uparse.urlencode(params)
        self._request.full_url = base_path + '?' + query_string
        self._request.add_header('Content-type', 'message/http')

    def list_objects(self, path, continuation_token=None):
        response = self._list_objects(path, continuation_token)
        page = minidom.parse(response)
        next_token = page.getElementsByTagName('NextContinuationToken')
        if next_token:
            token = next_token[0].firstChild.data
            return [page] + self.list_objects(path, token)
        return [page]
