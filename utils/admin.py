from django.contrib import messages
from django.contrib.admin import (
    ACTION_CHECKBOX_NAME, ModelAdmin, helpers, SimpleListFilter
)
from django.contrib.admin.actions import delete_selected
from django.contrib.admin.utils import get_deleted_objects, model_ngettext
from django.core.exceptions import PermissionDenied
from django.db import router
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.translation import ugettext as _, ugettext_lazy

from .forms import TreeModelForm


class TreeAdmin(ModelAdmin):
    move_selected_template = None
    form = TreeModelForm

    def get_queryset(self, request):
        """
        Returns a HierarchicalQuerySet of all model
        instances that can be edited by the admin.
        """
        qs = self.model.hierarchy
        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def delete_tree(self, model_admin, request, queryset):
        """
        Custom delete_selected action that does not
        display closure tables on confirmation page.
        """
        template_response = delete_selected(model_admin, request, queryset)
        if not request.POST.get('post'):
            selected = request.POST.getlist(ACTION_CHECKBOX_NAME)
            selected = [int(x) for x in selected]
            template_response = self._remove_auxiliary_data(template_response,
                                                            selected)
        return template_response

    def render_delete_form(self, request, context):
        template_response = super().render_delete_form(request, context)
        object_id = context["object"].pk
        return self._remove_auxiliary_data(template_response, [object_id])

    def move_tree(self, request, queryset):
        """
        Action that allows selection and assignment
        of new parent for selected nodes.
        """
        opts = self.model._meta
        app_label = opts.app_label

        # Check that the user has change permission for the actual model
        if not self.has_change_permission(request):
            raise PermissionDenied

        using = router.db_for_write(self.model)

        # Populate deletable_objects, a data structure of all related objects that
        # will also be deleted.
        deletable_objects, model_count, perms_needed, protected = get_deleted_objects(
            queryset, opts, request.user, self.admin_site, using)

        # The user has already confirmed the deletion.
        # Do the deletion and return a None to display the change list view again.
        if request.POST.get('post') and not protected:
            new_parent = int(request.POST.get('new_parent'))
            n = queryset.count()
            if n:
                for obj in queryset:
                    log_message = [{'changed': {'fields': ['parent_id']}}]
                    self.log_change(request, obj, log_message)
                    obj.parent_id = new_parent
                    obj.save()
                message = 'Успешно изменено {count} {items}'.format(
                    count=n, items=model_ngettext(self.opts, n))
                self.message_user(request, message, messages.SUCCESS)
            # Return None to display the change list page again.
            return None

        if len(queryset) == 1:
            objects_name = force_text(opts.verbose_name)
        else:
            objects_name = force_text(opts.verbose_name_plural)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": objects_name}
        else:
            title = _("Are you sure?")

        model = self.model
        movable_objects = model.hierarchy.add_root(pk__in=queryset)
        potential_parents = model.hierarchy.exclude(pk__in=movable_objects)

        model_count = self._remove_closure_from_count(
            dict(model_count).items())
        context = dict(
            self.admin_site.each_context(request),
            title=title,
            objects_name=objects_name,
            movable_objects=movable_objects,
            potential_parents=potential_parents,
            model_count=model_count,
            queryset=queryset,
            perms_lacking=perms_needed,
            protected=protected,
            opts=opts,
            action_checkbox_name=helpers.ACTION_CHECKBOX_NAME,
            media=self.media,
        )

        request.current_app = self.admin_site.name

        # Display the confirmation page
        return TemplateResponse(request, self.move_selected_template or [
            "admin/%s/%s/move_selected.html" % (app_label, opts.model_name),
            "admin/%s/move_selected.html" % app_label,
            "admin/move_selected.html"
        ], context)
    move_tree.short_description = ugettext_lazy(
        "Переместить выбранные %(verbose_name_plural)s")

    def _remove_auxiliary_data(self, template_response, selected):
        if not selected:
            return template_response
        elif len(selected) == 1:
            condition = {'pk': selected[0]}
        else:
            condition = {'pk__in': selected}

        deletable_objects = self.model.hierarchy.add_root(
            **condition).order_by('name')
        model_count = template_response.context_data['model_count']
        model_count = self._remove_closure_from_count(model_count)
        template_response.context_data.update(
            {'deletable_objects': deletable_objects,
             'model_count': model_count})
        return template_response

    def _remove_closure_from_count(self, model_count):
        return [x for x in model_count if not (x[0].endswith('closures')
                                               or x[0].endswith('closure'))]

    def get_actions(self, request):
        actions = super().get_actions(request)
        _, name, description = actions['delete_selected']
        actions['delete_selected'] = self.delete_tree, name, description
        return actions

    actions = ['move_tree']


class VisibleTreeListFilter(SimpleListFilter):
    title = 'Видимость на сайте'

    def lookups(self, request, model_admin):
        return [
            (1, 'Видимые'),
            (0, 'Скрытые')
        ]

    def queryset(self, request, queryset):
        value = self.value()
        if value is None:
            return queryset

        if int(value) == 1:
            queryset = queryset.filter_tree(visible=True)
        else:
            queryset = queryset.exclude_tree(visible=True)
        return queryset


def redirect_to_intermidiate_page(request, view_name):
    selected = request.POST.getlist(ACTION_CHECKBOX_NAME)
    view_url = reverse('admin:' + view_name)
    redirect_url = view_url + '?objects={}'.format(','.join(selected))
    return HttpResponseRedirect(redirect_url)

def redirect_to_change_list(model):
    view_name = '{}_{}_changelist'.format(
        model._meta.app_label, model._meta.model_name)
    redirect_url = reverse('admin:' + view_name)
    return HttpResponseRedirect(redirect_url)
