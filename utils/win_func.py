from django.db.models.expressions import Func


class WinFunc(Func):
    """
    Base class for window functions.
    """
    template = ('%(function)s(%(expressions)s)'
                ' OVER(%(partition_by)s%(order_by)s)')
    def __init__(self, *expressions, **extra):
        # TODO: check that "function" argument is instance of either
        # this class or Aggregate
        # TODO: teach class how to handle Aggregate functions
        # TODO: add support for changing directions of order_by
        # TODO: add check for partition_by argument
        partition_by = extra.pop('partition_by', None)
        order_by = extra.pop('order_by', None)
        #order_by = order_by if order_by else None
        if (hasattr(partition_by, '__iter__')
                and not isinstance(partition_by, str)):
            msg = 'PARTITION BY accepts exactly one column, {} given'
            raise TypeError(msg.format(len(partition_by)))
        if partition_by is not None:
            partition_by = self._parse_expressions(partition_by)
        self._clause_data = {
            'partition_by': {'template': 'PARTITION BY {}',
                             'args': partition_by},
            'order_by': {'template': 'ORDER BY {}',
                         'args': order_by if order_by else None},
        }
        super().__init__(*expressions, **extra)

    def as_sql(self, compiler, connection, function=None, template=None,
               arg_joiner=None, **extra_context):
        clause_sql, clause_params = self._make_over_clause(compiler)
        extra_context.update(clause_sql)
        function_sql, function_params = super().as_sql(
                compiler, connection, function=function,
                template=template if template is not None else self.template,
                arg_joiner=arg_joiner, **extra_context)
        function_params.extend(clause_params)
        return (function_sql, function_params)

    def _make_over_clause(self, compiler):
        sql_parts = {}
        params = []
        for key, part_data in self._clause_data.items():
            args = part_data['args']
            if args is None:
                sql_parts[key] = ''
            else:
                compiled_sql = []
                for arg in args:
                    if key == 'order_by':
                        # arg[0] contains OrderBy object
                        part_sql, part_params, ref = arg[1]
                    else:
                        part_sql, part_params = compiler.compile(arg)
                    compiled_sql.append(part_sql)
                    params.extend(part_params)
                joined_sql = self.arg_joiner.join(compiled_sql)
                sql_parts[key] = part_data['template'].format(joined_sql)
        if all(sql_parts.values()):
            sql_parts['partition_by'] += ' '
        return (sql_parts, params)

    def resolve_expression(self, query=None, allow_joins=True, reuse=None,
                           summarize=False, for_save=False):
        cd = self._clause_data
        partition_by = cd['partition_by']['args']
        if partition_by is not None:
            cd['partition_by']['args'] = [partition_by[0].resolve_expression(
                query, allow_joins, reuse, summarize, for_save)]

        if cd['order_by']['args'] is not None:
            args = cd['order_by']['args']
            # if: single item, else: list of items
            if isinstance(args, str) or not hasattr(args, '__iter__'):
                query.add_ordering(args)
            else:
                query.add_ordering(*args)
            default_db = query.model._default_manager.db
            order_args = query.get_compiler(using=default_db).get_order_by()
            cd['order_by']['args'] = order_args
            query.clear_ordering(True)
        return super().resolve_expression(
            query=query, allow_joins=allow_joins, reuse=reuse,
            summarize=summarize, for_save=for_save)


class ArrayAggMult(WinFunc):
    """
    Collects values of specified column(s) into array.
    """
    function = 'ARRAY_AGG_MULT'

    def __init__(self, *expressions, **extra):
        if len(expressions) == 0:
            raise TypeError(
                '"{}" must have at least one argument'.format(
                    self.__class__.__name__)
            )
        return super().__init__(*expressions, **extra)


class DenseRank(WinFunc):
    """
    Numbers rows inside partition
    """
    function = 'DENSE_RANK'
    arity = 0
