import re

from django import template
from django.utils.safestring import mark_safe
from more_itertools import peekable


register = template.Library()

@register.tag
def tree(parser, token):
    try:
        tag_name, args = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError(
            '{} tag requires arguments'.format(token.contents.split()[0])
        )
    m = re.search(r'(.*?) as (\w+), (\w+)', args)
    if not m:
        raise template.TemplateSyntaxError(
            "{0} tag should be of format '{0} x as y, z'".format(tag_name))
    node_list, branch_name, node_name = m.groups()
    nodelist_branch = parser.parse(('leaf', 'endtree',))
    token = parser.next_token()
    if token.contents == 'leaf':
        nodelist_leaf = parser.parse(('endtree',))
        parser.delete_first_token()
    else:
        raise template.TemplateSyntaxError("Missing leaf tag")
    return TreeNode(node_list, branch_name, node_name,
                    nodelist_branch, nodelist_leaf)

class TreeNode(template.Node):
    def __init__(self, node_list, branch_name, node_name,
                 nodelist_branch, nodelist_leaf):
        self.node_list = template.Variable(node_list)
        self.branch_name = branch_name
        self.node_name = node_name
        self.nodelist_branch = nodelist_branch
        self.nodelist_leaf = nodelist_leaf

    def __repr__(self):
        return '<TreeNode>'

    def render(self, context):
        tree = self._build_tree(context)
        return ''.join((self._rec_render(context, x) for x in tree))

    def _rec_render(self, context, node):
        children = node['children']
        node = node['node']
        if children:
            bits = []
            for child in children:
                with context.push():
                    context[self.node_name] = child['node']
                    bits.append(self._rec_render(context, child))
            branches = ''.join(bits)
            with context.push():
                context[self.node_name] = node
                context[self.branch_name] = mark_safe(branches)
                output = self.nodelist_branch.render(context)
        else:
            with context.push():
                context[self.node_name] = node
                output = mark_safe(
                    self.nodelist_leaf.render(context))
        return output

    def _build_tree(self, context):
        node_list = self.node_list.resolve(context)
        if not node_list:
            return []
        self.parent_attr = node_list[0]._closure_sentinel_attr + '_id'
        node_list = peekable(node_list)
        while True:
            sub_tree = self._rec_build_tree(node_list)
            if sub_tree:
                yield sub_tree[0]
            else:
                break

    def _rec_build_tree(self, node_list):
        group = []
        group_pks = []
        for node in node_list:
            group.append({'node': node, 'children': []})
            group_pks.append(node.pk)
            try:
                self.parent_id = getattr(node_list.peek(), self.parent_attr)
            except StopIteration:
                break
            if self.parent_id in group_pks:
                group[-1]['children'] = self._rec_build_tree(node_list)
            elif self.parent_id != getattr(node, self.parent_attr):
                return group
            if self.parent_id is None:
                break
        return group
