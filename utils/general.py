from itertools import chain


class NodeTreeGenerator:
    def __init__(self, per_node, max_depth):
        self.per_node = per_node
        self.max_depth = max_depth

    def generate(self, sample, name_attr='name', parent_attr='parent'):
        total_nodes = sum([self.per_node**x for x in range(self.max_depth)])
        node_cls = sample.__class__
        node_cls.objects.bulk_create([sample]*total_nodes)
        self._nodes = node_cls.objects.all().order_by('-id')[:total_nodes]
        self._link(parent_attr)
        self._name_nodes(name_attr)

        for node in self._nodes:
            node.save()

    def _name_nodes(self, name_attr):
        for node in self._nodes:
            new_name = '{0}-{1}'.format(getattr(node, name_attr), node.pk)
            setattr(node, name_attr, new_name)

    def _link(self, parent_attr):
        leaves_amount = self.per_node ** (self.max_depth-1)
        branch_amount = len(self._nodes) - leaves_amount
        parents = chain.from_iterable(([node]*self.per_node
                                       for node
                                       in self._nodes[:branch_amount]))
        for parent, child in zip(parents, self._nodes[1:]):
            setattr(child, parent_attr, parent)


def chunk_by_var(nodes, count_list):
    nodes_iter = iter(nodes)
    return [[next(nodes_iter) for x in range(amount)]
            for amount in count_list]
