from django.db.models import (
    BigIntegerField, Exists, OuterRef, QuerySet, Subquery)
from django.db.models.expressions import Col, F, OrderBy, RawSQL
from django.db.models.functions.base import Cast
from django.db.models.lookups import Exact
from django.db.models.query_utils import Q
from django.db.models.sql.where import AND

from .win_func import ArrayAggMult, DenseRank


class HierarchicalQuerySet(QuerySet):
    """QuerySet for working with tree-like structures.

    Applies all ordering to sibling elements instead of of whole query
    while preserving tree structure.
    Use set_root to add filtering for specific ancestor(s)
    Use regular filter/exclude to filter by parameter(s) that
    should be the same for entire tree/subtree (e.g. menu_id for
    menu items or post_id for comments).
    Use filter_tree/exclude_tree to filter by parameter(s) that
    will vary from element to element.
    """

    def __init__(self, *args, **kwargs):
        self._condition_count = 0
        super().__init__(*args, **kwargs)

    def _clone(self, *args, **kwargs):
        clone = super()._clone(*args, **kwargs)
        clone._condition_count = self._condition_count
        return clone

    def _fetch_all(self):
        if self._result_cache is None:
            self._result_cache = list(
                self._iterable_class(self._make_tree_qs()))
        if self._prefetch_related_lookups and not self._prefetch_done:
            self._prefetch_related_objects()

    def _get_limits(self):
        return {'low': self.query.low_mark, 'high': self.query.high_mark}

    def _make_tree_qs(self):
        flat_tree = self._from_subquery(self._default_manager.get_queryset(),
                                        self._make_path_qs())
        depth_field = self.model._closure_model._meta.get_field('depth')
        depth_col = Col(flat_tree.query.alias_prefix, depth_field)
        flat_tree.query.where.add(Exact(depth_col, 0), AND)
        flat_tree = flat_tree.order_by('path')
        flat_tree = flat_tree.annotate(path=self._get_col('path'),
                                       **self._get_user_annotations())
        flat_tree.query.set_limits(**self._get_limits())
        return flat_tree

    def _make_path_qs(self):
        select_fields = [self._get_col(x.column) for x
                        in self.query.get_meta().fields]
        select_fields.append(self._get_col('depth'))
        select_fields.extend(self._get_user_annotations().values())
        path_qs = self._default_manager.annotate(
            path=ArrayAggMult(
                Cast(self._get_col('parent_id'), BigIntegerField()),
                Cast(self._get_col('sib_order'), BigIntegerField()),
                partition_by=self._get_col('child_id'),
                order_by=OrderBy(self._get_col('depth'), descending=True))
        )
        path_qs = self._from_subquery(path_qs, self._make_sib_order_qs(),
                                      select=select_fields)
        return path_qs

    def _make_sib_order_qs(self):
        closure_ref = self.model._closure_parentref() + '__'
        clone = self._clone()
        # Resetting limit and ofset, they will be set on outermost query
        clone.query.low_mark = None
        clone.query.high_mark = None
        sib_order_qs = clone.annotate(
            sib_order=DenseRank(
                partition_by='parent_id', order_by=self._ordering,
                output_field=BigIntegerField()),
            child_id=F(closure_ref + 'child_id'),
            depth=F(closure_ref + 'depth')
        )
        return sib_order_qs

    def _from_subquery(self, outer_query, inner_query, select=[]):
        tb_alias = self.query.alias_prefix
        subqs = Subquery(inner_query)
        subqs.template += ' {}'.format(tb_alias)
        query = outer_query.query
        query.tables = [tb_alias]
        query.alias_refcount[tb_alias] = 1
        query.alias_map[tb_alias] = subqs
        if select:
            query.set_select(select)
        return outer_query

    def _get_col(self, col_name):
        return RawSQL(col_name, ())

    def _get_user_annotations(self):
        """Turns user-specified annotations into raw column names"""
        return {k: self._get_col(k) for k in self.query.annotations.keys()}

    def filter_tree(self, *args, for_all=True, **kwargs):
        """Filters out entire subtrees by parameters specified.

        If for_all is true and element does not match search criteria
        the element and all of it's subelements will not be included
        into result set.
        If for_all is false element will be included into result set
        if at least one of the ancestors does match search criteria.
        """
        return self._filter_or_exclude_tree(
            False, *args, for_all=for_all, **kwargs)

    def exclude_tree(self, *args, for_all=True, **kwargs):
        """Excluding version of filter_tree."""
        return self._filter_or_exclude_tree(
            True, *args, for_all=for_all, **kwargs)

    def _filter_or_exclude_tree(self, negate, *args, for_all=True, **kwargs):
        self._condition_count += 1
        closure_ref = self.model._closure_parentref() + '__child_id'
        outer_ref = {closure_ref: OuterRef('pk')}
        sub_qs = self._default_manager.get_queryset()
        if for_all:
            args_negated = [~Q(x) for x in args]
            kwargs_negated = [~Q(**{k:v}) for k, v in kwargs.items()]
            sub_qs = sub_qs.filter(*args_negated + kwargs_negated, **outer_ref)
            subquery = ~Exists(sub_qs)
        else:
            sub_qs = sub_qs.filter(*args, **outer_ref, **kwargs)
            subquery = Exists(sub_qs)
        filter_tree = 'filter_tree{}'.format(self._condition_count)
        clone = self._clone()
        clone = clone.annotate(**{filter_tree: subquery})
        if negate:
            clone = clone.exclude(**{filter_tree: True})
        else:
            clone = clone.filter(**{filter_tree: True})
        return clone

    def add_root(self, *args, **kwargs):
        qs = self._default_manager.get_queryset().filter(*args, **kwargs)
        closure_ref = self.model._closure_parentref() + '__parent_id'
        return self.filter_tree(**{closure_ref+'__in': qs}, for_all=False)

    @property
    def _default_manager(self):
        return self.query.get_meta().default_manager

    @property
    def _ordering(self):
        query = self.query
        if query.extra_order_by:
            ordering = query.extra_order_by
        elif not query.default_ordering:
            ordering = query.order_by
        else:
            ordering = (query.order_by or query.get_meta().ordering or [])
        return ordering if ordering else [self.query.get_meta().pk.column]
