from django.contrib.admin import AdminSite, site as django_site


class StoreAdminSite(AdminSite):
    site_header = 'Управление проектом'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._registry = django_site._registry


site = StoreAdminSite(name='admin')
