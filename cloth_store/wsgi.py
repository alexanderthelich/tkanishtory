"""
WSGI config for cloth_store project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import mod_wsgi
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      "cloth_store.settings.{}".format(mod_wsgi.process_group))

def application(environ, start_response):
    for k,v in environ.items():
        os.environ.setdefault(k, str(v))
    return get_wsgi_application()(environ, start_response)
