from django.http import Http404
from django.views.generic.detail import DetailView

from .models import Page


class PageDetailView(DetailView):
    model = Page

    def get_object(self, *args, **kwargs):
        obj = super().get_object(*args, **kwargs)
        if not obj.visible:
            raise Http404('Page not found')
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page_crumb = {'name': self.object.name, 'url': None}
        context['breadcrumbs'] = (page_crumb,)
        return context


class IndexView(DetailView):
    model = Page
    template_name = 'misc_pages/index.html'


class ContactsView(PageDetailView):
    template_name = 'misc_pages/contacts.html'
