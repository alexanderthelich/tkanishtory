from django.conf.urls import url

from . import views

app_name = 'misc_pages'
urlpatterns = [
    url(r'^index/$', views.IndexView.as_view(),
        {'slug': 'index'}, name='index'),
    url(r'^contact_info/$', views.ContactsView.as_view(),
        {'slug': 'contact_info'}, name='contacts'),
    url(r'^(?P<slug>[-\w]+)/$', views.PageDetailView.as_view(), name='detail'),
]
