from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.urls import reverse
from django.utils.html import format_html
from tinymce import HTMLField

from menus.models import MenuItem

class Page(models.Model):
    slug = models.SlugField(
        'Код страницы', unique=True,
        help_text='Только латинские буквы, цифры, дефисы и нижние подчёркивания'
    )
    name = models.CharField('Имя страницы', max_length=200)
    text_content = HTMLField('Текст страницы', blank=True)
    title = models.CharField('Заголовок', max_length=200)
    keywords = models.CharField('Ключевые слова', max_length=200, blank=True)
    description = models.TextField('Описание', blank=True)
    visible = models.BooleanField('Страница доступна пользователям', default=False)
    menu_items = GenericRelation(MenuItem, related_query_name='categories')

    class Meta:
        verbose_name = 'страница'
        verbose_name_plural = 'страницы'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('misc_pages:detail', kwargs={'slug': self.slug})
    get_absolute_url.short_description = 'Ссылка страницы'

    @property
    def menu_link(self):
        return self.get_absolute_url() if self.slug else ''

    def site_link(self):
        html = '<a target="_blank" href="{}">Ссылка</a>'
        return format_html(html, self.get_absolute_url())
    site_link.short_description = 'Ссылка на сайте'
