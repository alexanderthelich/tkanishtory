from django.apps import AppConfig


class MiscPagesConfig(AppConfig):
    name = 'misc_pages'
    verbose_name = 'Прочие страницы'
