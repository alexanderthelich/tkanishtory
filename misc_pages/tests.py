from django.test import TestCase
from django.urls import reverse

from .models import Page

class PageIndexTest(TestCase):
    def test_index_page_exists(self):
        """
        Project index page exists, uses right template and returns
        expected content
        """
        Page.objects.create(slug='index', text_content='Index page test content',
                            visible=1)
        response = self.client.get(reverse('misc_pages:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Index page test content')
        self.assertTemplateUsed('misc_pages/index.html')


class PageDetailView(TestCase):
    def test_page_does_exist(self):
        """
        Page does exist, uses right template and returns expected content
        """
        Page.objects.create(slug='test', text_content='This is test page',
                            visible=1)
        response = self.client.get(reverse('misc_pages:detail', kwargs={'slug': 'test'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'This is test page')
        self.assertTemplateUsed('misc_pages/page_detail.html')

    def test_hidden_page_is_not_available(self):
        """
        If page is hidden, it can only be accessed from admin site.
        """
        Page.objects.create(slug='test', text_content='This is test page',
                            visible=0)
        response = self.client.get(reverse('misc_pages:detail', kwargs={'slug': 'test'}))
        self.assertEqual(response.status_code, 404)
