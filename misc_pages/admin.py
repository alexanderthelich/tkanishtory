from django.contrib import admin

from cloth_store.admin import site
from menus.forms import GenericMenuItemInline

from .models import Page


class PageAdmin(admin.ModelAdmin):
    inlines = [GenericMenuItemInline]
    list_display = ('name', 'visible', 'site_link',)
    fieldsets = [
        (None, {
            'fields': ('slug', 'name', 'title', 'text_content', 'visible',)
        }),
        ('SEO-поля', {
            'fields': ('keywords', 'description'),
            'classes': ('collapse',),
        }),
    ]

site.register(Page, PageAdmin)
