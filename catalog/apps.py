from django.apps import AppConfig
from django.db.models.signals import post_delete


class CatalogConfig(AppConfig):
    name = 'catalog'
    verbose_name = 'Каталог'

    def ready(self):
        from .models import WareImage, WareThumbnail
        from .signals import image_cleaner
        post_delete.connect(image_cleaner, sender=WareImage,
                            dispatch_uid='cleanup_orphan_image')
        post_delete.connect(image_cleaner, sender=WareThumbnail,
                            dispatch_uid='cleanup_orphan_thumbnail')
