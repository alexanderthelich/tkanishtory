from datetime import datetime

from django.conf.urls import url
from django.contrib.admin import HORIZONTAL, VERTICAL, ModelAdmin
from django.contrib.admin.filters import SimpleListFilter
from django.contrib import messages
from django.forms.models import modelformset_factory
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe

from cloth_store.admin import site
from menus.forms import GenericMenuItemInline
from utils.admin import (
    redirect_to_intermidiate_page, redirect_to_change_list,
    VisibleTreeListFilter, TreeAdmin
)

from .exporters import CashDeskExporter
from .forms import *
from .models import (Category, Ware, WareProperty, WareThumbnail, Sale)


class VisibleCategoryListFilter(VisibleTreeListFilter):
    parameter_name = 'category_visible'


class CategoryAdmin(TreeAdmin):
    inlines = [CategoryInline, GenericMenuItemInline]
    list_filter = (VisibleCategoryListFilter,)
    list_display = ('depth_padded_name', 'visible', 'site_link',)
    search_fields = ['name']
    fieldsets = [
        (None, {
            'fields': ('id', 'slug', 'parent', 'name', 'title', 'visible',)
        }),
        ('Тексты', {
            'fields': ('text_content', 'ware_default_text',),
            'classes': ('collapse',),
        }),
        ('SEO-поля', {
            'fields': ('keywords', 'description'),
            'classes': ('collapse',),
        }),
    ]
    readonly_fields = ('id',)


class WareCategoryListFilter(SimpleListFilter):
    parameter_name = 'category'
    title = 'Категория товара'
    template = 'admin/catalog/ware/filter_category.html'

    def lookups(self, request, model_admin):
        return Category.get_choices_list()

    def choices(self, changelist):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == force_text(lookup),
                'option_value': lookup,
                'option_name': title,
            }

    def queryset(self, request, queryset):
        value = self.value()
        if not value:
            return queryset
        else:
            category_model = queryset.model.category.field.related_model
            category_filter = 'category__{}__parent_id'.format(
                category_model.closure_childref())
            return queryset.filter(**{category_filter: value})


class WareAdmin(ModelAdmin):
    save_as = True
    actions = ['add_discount_action', 'set_main_category_action', 
               'export_as_csv_action']
    inlines = [WareImageInline, WarePropertyValueInline, WareDiscountInline]
    list_filter = (WareCategoryListFilter,)
    list_display = ('name', 'current_image', 'price',
                    'derived_product_code', 'visible')
    list_editable = ('price', 'visible')
    form = WareAdminModelForm
    radio_fields = {'units': HORIZONTAL, 'main_category': VERTICAL}
    search_fields = ['name', 'pk']
    fieldsets = [
        (None, {
            'fields': ('category', 'main_category', 'name', 'price',
                       'product_code_prefix', 'units', 'new_until', 'title',
                       'text_content', 'visible')
        }),
        ('SEO-поля', {
            'fields': ('keywords', 'description'),
            'classes': ('collapse',),
        }),
    ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.main_image(WareThumbnail.TINY)

    def get_urls(self):
        urls = super().get_urls()
        new_urls = [
            url(r'ware_to_sale/$', self.admin_site.admin_view(
                self.add_discount_view), name='add_discount'),
            url(r'set_main_category/$', self.admin_site.admin_view(
                self.set_main_category_view), name='set_main_category'),
        ]
        return new_urls + urls

    def current_image(self, obj):
        image_tag = '<img src="{}" style="max-width: 64px;'\
            ' max-height: 64px;" />'
        return mark_safe(image_tag.format(obj.main_image_url))
    current_image.short_description = 'Изображение'

    def add_discount_action(self, request, queryset):
        return redirect_to_intermidiate_page(request, 'add_discount')
    add_discount_action.short_description = 'Добавить скидку'

    def set_main_category_action(self, request, queryset):
        return redirect_to_intermidiate_page(request, 'set_main_category')
    set_main_category_action.short_description = 'Изменить главную категорию'

    def export_as_csv_action(self, request, queryset):
        response = HttpResponse(content_type='text/csv')
        date_time_string = datetime.today().isoformat(timespec='seconds')
        filename = ''.join(['ware_export_', date_time_string, '.csv'])
        response['Content-Disposition'] = f'attachment; filename="{filename}"'
        exporter = CashDeskExporter()
        exporter.export_query_csv(queryset, response)
        return response
    export_as_csv_action.short_description = 'Экспортировать в csv'

    def add_discount_view(self, request):
        context = dict(self.admin_site.each_context(request))
        wares_pks = request.GET.get('objects', []).split(',')
        wares = self.model.objects.filter(
            pk__in=wares_pks).main_image(WareThumbnail.TINY)
        discount_initial = [{'ware': x.pk, 'ware_name': x.name,
                             'ware_img': self.current_image(x)} for x in wares]
        discount_formset_class = modelformset_factory(
            WareDiscount, extra=len(discount_initial),
            form=SaleWareDiscountModelForm
        )
        if request.method == "POST":
            discount_formset = discount_formset_class(
                request.POST, initial=discount_initial,
                queryset=WareDiscount.objects.none()
            )
            if discount_formset.is_valid():
                saved_objects = discount_formset.save()
                msg = 'Добавлены скидки для {} товаров.'.format(
                    len(saved_objects))
                self.message_user(request, msg, level=messages.SUCCESS)
                return redirect_to_change_list(self.model)
        else:
            discount_formset = discount_formset_class(
                initial=discount_initial, queryset=WareDiscount.objects.none())
        extra_context = {
            'title': 'Добавление товара в распродажу',
            'cl': self,
            'sales': Sale.objects.unfinished(),
            'discount_formset': discount_formset,
        }
        context.update(extra_context)
        return TemplateResponse(
            request, 'admin/catalog/ware/ware_to_sale.html', context)

    def set_main_category_view(self, request):
        if request.method == "POST":
            form = ChangeWareCategoryForm(request.POST)
            if form.is_valid():
                self._replace_main_category(request, form)
                msg = 'Главная категория изменена.'
                self.message_user(request, msg, level=messages.SUCCESS)
                return redirect_to_change_list(self.model)
        context = dict(self.admin_site.each_context(request))
        form = ChangeWareCategoryForm()
        form['category'].label = 'Новая главная категория'
        extra_context = {
            'title': 'Изменение главной категории для товаров',
            'cl': self,
            'form': form,
        }
        context.update(extra_context)
        return TemplateResponse(
            request, 'admin/catalog/ware/set_main_category.html', context)

    def _replace_main_category(self, request, form):
        ware_ids = request.GET.getlist('objects')
        new_category = form.cleaned_data['category']
        selected_wares = self.model.objects.filter(pk__in=ware_ids)
        old_categories = selected_wares.values_list(
            'main_category', flat=True).distinct()
        link_table = self.model.category.through
        link_table.objects.filter(
            ware_id__in=ware_ids, category_id__in=old_categories).delete()
        link_table.objects.bulk_create(
            link_table(ware_id=x, category_id=new_category) for x in ware_ids)
        # updating Wares as last thing to prevent wrong
        # results in old_categorie due to lazy evaluations
        selected_wares.update(main_category=new_category)


class WarePropertyAdmin(ModelAdmin):
    list_display = ('name',)
    list_display_links = None
    list_editable = ('name',)


class SaleTimelineListFilter(SimpleListFilter):
    parameter_name = 'timeline'
    title = 'Состояние'
    valid_lookups = ('active', 'future', 'unfinished', 'finished')

    def lookups(self, request, model_admin):
        return (
            ('active', 'Активные'),
            ('future', 'Будущие'),
            ('unfinished', 'Активные и будущие'),
            ('finished', 'Завершённые'),
        )

    def queryset(self, request, queryset):
        timeline_type = self.value()
        if timeline_type and timeline_type in self.valid_lookups:
            timeline_filter = getattr(queryset, timeline_type)
            return timeline_filter()
        else:
            return queryset


class SaleAdmin(ModelAdmin):
    inlines = [SaleWareDiscountInline]
    fieldsets = [
        (None, {
            'fields': ('name', 'title', 'timeline', 'text_content')
        }),
        ('SEO-поля', {
            'fields': ('keywords', 'description'),
            'classes': ('collapse',),
        }),
    ]
    list_display = ('name', 'start_date', 'end_date')
    list_filter = (SaleTimelineListFilter,)

    def start_date(self, obj):
        return obj.timeline.lower.strftime(obj.date_format)
    start_date.short_description = 'Начало'

    def end_date(self, obj):
        return obj.timeline.upper.strftime(obj.date_format)
    end_date.short_description = 'Окончание'


site.register(Category, CategoryAdmin)
site.register(Ware, WareAdmin)
site.register(WareProperty, WarePropertyAdmin)
site.register(Sale, SaleAdmin)
