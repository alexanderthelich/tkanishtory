from django.conf.urls import url

from . import views

app_name = 'catalog'
urlpatterns = [
    url(r'^main_category/$', views.main_category, name='main_category_radio'),
    url(r'^ware_images/$', views.ware_images, name='ware_images'),
    url(r'^discounts/non-sale/$', views.DiscountListView.as_view(),
        name='discounts_non_sale'),
    url(r'^discounts/all/$', views.AllDiscountListView.as_view(),
        name='discounts_all'),
    url(r'^new-wares/$', views.NewWaresListView.as_view(), name='new_wares'),
    url(r'^sale/(?P<pk>[0-9]+)/$', views.SaleDetailView.as_view(),
        name='sale_detail'),
    url(r'^(?P<slug>[-\w]+)/$', views.CategoryDetailView.as_view(),
        name='detail'),
    url(r'^(?P<slug>[-\w]+)/list/$', views.CategoryListView.as_view(),
        name='list'),
    url(r'^(?P<category_slug>[-\w]+)/(?P<pk>[0-9]+)/$',
        views.WareDetailView.as_view(), name='ware_detail'),
]
