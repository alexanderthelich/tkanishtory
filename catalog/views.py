import datetime

from django.db.models import Q
from django.forms.widgets import RadioSelect
from django.http import Http404, JsonResponse
from django.views.generic import ListView
from django.views.generic.detail import DetailView, SingleObjectMixin
from django.urls import reverse

from .forms import WareMainCategoryChoiceField
from .models import Category, Sale, Ware, WareImage, WareThumbnail


WARE_PER_PAGE = 21


def get_breadcrumbs(instance):
    ancestors = instance.get_ancestors()
    ancestors = ancestors.order_by(
        '-{}__depth'.format(instance._closure_parentref()))
    crumbs = []
    for category in ancestors:
        crumb = {
            'name': category.name,
            'url': reverse('catalog:list', kwargs={'slug': category.slug})
        }
        crumbs.append(crumb)
    return crumbs


class BaseWareListView(ListView):
    paginate_by = WARE_PER_PAGE

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['breadcrumbs'] = self._get_breadcrumbs()
        context['ware_list'] = context['object_list']
        return context

    def get_queryset(self):
        return Ware.objects.ware_user_list(self._filters)


class CategoryDetailView(SingleObjectMixin, BaseWareListView):
    template_name = 'catalog/category_detail.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Category.objects.all())
        if not self.object.is_visible():
            raise Http404('Page not found')
        return super().get(request, *args, **kwargs)

    def _get_breadcrumbs(self):
        crumbs = get_breadcrumbs(self.object)
        crumbs.append({'name': self.object.name, 'url': None})
        return crumbs

    def get_queryset(self):
        filters = {
            'visible': True,
            'category__{}__parent_id'.format(
                self.object.closure_childref()): self.object.pk,
        }
        return Ware.objects.ware_user_list(filters)


class CategoryListView(ListView):
    def get_queryset(self):
        """ Showing immediate children of selected category. """
        return Category.objects.filter(visible=True,
                                       parent__slug=self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['breadcrumbs'] = self._get_breadcrumbs()
        return context

    def _get_breadcrumbs(self):
        instance = Category.objects.get(slug=self.kwargs['slug'])
        crumbs = get_breadcrumbs(instance)
        crumbs.append({'name': instance.name, 'url': None})
        return crumbs


class WareDetailView(DetailView):
    model = Ware

    def get_object(self, *args, **kwargs):
        qs = self.model.objects.select_related('main_category')
        obj = super().get_object(*args, queryset=qs, **kwargs)
        if not obj.is_visible():
            raise Http404('Page not found')
        return obj

    def _get_detail_images(self, ware):
        is_main_image = Q(original_image__main_image=True,
                          size_type=WareThumbnail.MEDIUM)
        is_secondary_image = Q(original_image__main_image=False,
                               size_type=WareThumbnail.EXTRA_SMALL)
        size_condition = is_main_image | is_secondary_image
        images = WareThumbnail.objects.filter(
            size_condition, original_image__ware=ware)
        return images.order_by('-original_image__main_image')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ware = context['ware']
        images = self._get_detail_images(ware)
        extra_context = {
            'main_image': images[0] if images else None,
            'dummy_image': WareImage.DUMMY,
            'secondary_images': images[1:] if images else [],
            'ware_properties': ware.properties.select_related('property'),
            'breadcrumbs': self._get_breadcrumbs(),
        }
        context.update(extra_context)
        return context

    def _get_breadcrumbs(self):
        instance = Category.objects.get(slug=self.kwargs['category_slug'])
        crumbs = get_breadcrumbs(instance)
        crumbs.append(
            {'name': instance.name,
             'url': reverse('catalog:detail', kwargs={'slug': instance.slug})}
        )
        crumbs.append({'name': self.object.name, 'url': None})
        return crumbs


class SaleDetailView(SingleObjectMixin, BaseWareListView):
    template_name = 'catalog/ware_list_base.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Sale.objects.all())
        return super().get(request, *args, **kwargs)

    def _get_breadcrumbs(self):
        crumbs = [
            {'name': 'Акции', 'url': reverse('catalog:discounts_all')},
            {'name': self.object.name, 'url': None},
        ]
        return crumbs

    def get_queryset(self):
        filters = {'discount__sale_id': self.object.pk, 'visible': True}
        return Ware.objects.ware_user_list(filters)


class DiscountListView(BaseWareListView):
    template_name = 'catalog/discount_ware_list.html'
    _filters = {'visible': True, 'discount__sale_id': None,
               'discount__pk__isnull': False}

    def _get_breadcrumbs(self):
        crumbs = [
            {'name': 'Акции', 'url': reverse('catalog:discounts_all')},
            {'name': 'Скидки', 'url': None},
        ]
        return crumbs


class AllDiscountListView(BaseWareListView):
    template_name = 'catalog/discount_ware_list.html'
    _filters = {'visible': True, 'discount__pk__isnull': False}

    def _get_breadcrumbs(self):
        crumbs = [
            {'name': 'Акции', 'url': reverse('catalog:discounts_all')},
            {'name': 'Все', 'url': None},
        ]
        return crumbs


class NewWaresListView(BaseWareListView):
    template_name = 'catalog/new_ware_list.html'
    _filters = {'visible': True, 'new_until__gt': datetime.date.today()}

    def _get_breadcrumbs(self):
        return [{'name': 'Новинки', 'url': None}]


def main_category(request):
    pks = request.GET.getlist('selected_categories[]')
    main_category = request.GET.getlist('main_category')
    categories = Category.objects.filter(pk__in=pks)
    field = WareMainCategoryChoiceField(
        categories, empty_label=None, widget=RadioSelect)
    response = field.widget.render('main_category', main_category)
    return JsonResponse({'html': response})


def ware_images(request):
    """ JSON view for obtaining links to ware images of certain size. """
    min_side_size = int(request.GET.get('size'))
    size_type = WareThumbnail.get_optimal_size_type(min_side_size)
    ware_id = int(request.GET.get('ware'))
    ware_images = WareThumbnail.objects.filter(
        size_type=size_type, original_image__ware_id=ware_id)
    response = {x.original_image_id: x.get_absolute_url() for x in ware_images}
    return JsonResponse({'ware_images': response})
