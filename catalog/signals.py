from django.db.models.signals import post_delete
from django.dispatch import receiver

from .models import WareImage


def image_cleaner(sender, **kwargs):
    kwargs['instance'].remove_image_file()
