var $ = django.jQuery;

function display_main_category_field() {
    var main_category_field = $('.field-main_category');
    if ($('#id_main_category input').length > 1) {
        main_category_field.show();
    } else {
        main_category_field.hide();
    }
}

$(document).ready(function() {
    display_main_category_field();
    $('#id_category').change(function () {
        var selected_categories = $(this).val();
        var main_category = $('#id_main_category input:checked').val()
        if (selected_categories) {
            var params = {
                'selected_categories': selected_categories,
                'main_category': main_category
            };
            $.get('/catalog/main_category/', params, function(data) {
                var new_contents = $(data['html']).contents();
                var category_selector = $('#id_main_category');
                category_selector.html(new_contents);
                var has_checked = category_selector.find('input:checked').length;
                if (!has_checked) {
                    // There should always be a selected category
                    category_selector.find('input').first().attr('checked', 'checked');
                }
                display_main_category_field();
            });
        };
    });
});
