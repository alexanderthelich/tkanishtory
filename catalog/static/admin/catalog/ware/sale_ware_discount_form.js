$(document).ready(function() {
    $('#ware-discounts').find('select[id$=discount_type]').each(function() {
        modify_discount_inputs($(this));
    });
    $('#ware-discounts').find('select[id$=discount_type]').change(function() {
        modify_discount_inputs($(this));
    });
    $('#ware-discounts').find('.inline-deletelink').click(function() {
        event.preventDefault();
        delete_discount_form($(this));
    });
    $('#form-selector').change(function() { select_all(this); });
    $('#discounts-multiple-set-button').click(function() {
        set_multiple();
    });
    $('#discounts-multiple-sale').change(function() {
        show_sale_timeline($(this));
    });
});

function set_conditon_availabilty(type_id, elem) {
    var condition_types = [3, 4, 5];
    var is_condition_type = (condition_types.indexOf(type_id) >= 0)
    if (is_condition_type) {
        elem.removeAttr('disabled');
    } else {
        elem.attr('disabled', '');
    }
}

function modify_discount_inputs(type_selector) {
    discount_type = parseInt(type_selector.val());
    var conditoin_input = type_selector.parent().siblings(
        'td.field-discount_condition').find('input');
    set_conditon_availabilty(discount_type, conditoin_input);
}

function delete_discount_form(delete_link) {
    var total_forms = $('#form-group').find('input[id$=TOTAL_FORMS]');
    total_forms.val(parseInt(total_forms.val()) - 1);
    var prefix = delete_link.attr('data-form-prefix');
    delete_link.closest('tr').remove();
    $('tr.'+prefix+'-non_field_errors').remove();
}

function select_all(form_selector) {
    // Note the _ instead of -
    $('input.form-selector').each(function() {
        this.checked = form_selector.checked;
    });
}

function set_multiple() {
    var discount_type = $('#discounts-multiple-type');
    var replacement = [
        ['sale', $('#discounts-multiple-sale')],
        ['discount_type', discount_type],
        ['discount_value', $('#discounts-multiple-value')],
        ['discount_condition', $('#discounts-multiple-condition')],
        ['timeline_0', $('#discounts-multiple-timeline-0')],
        ['timeline_1', $('#discounts-multiple-timeline-1')],
    ];
    $('input.form-selector:checked').each(function() {
        var current_row = $(this).closest('tr');
        for (i=0; i < replacement.length; i++) {
            var id = replacement[i][0];
            var new_val = replacement[i][1].val();
            var elem = current_row.find('[id$='+id+']');
            elem.val(new_val);
            if (id == 'discount_condition') {
                set_conditon_availabilty(parseInt(discount_type.val()), elem);
            }
        }
    });
}

function show_sale_timeline(sale) {
    var sale_id = sale.val();
    $('#sale-timelines p').addClass('sale-timeline');
    $('#sale-timeline-'+sale_id).removeClass('sale-timeline');
}
