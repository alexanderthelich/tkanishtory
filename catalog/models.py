import datetime
from hashlib import md5
from tempfile import NamedTemporaryFile
from time import time
import os

from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.postgres.aggregates.general import BoolAnd
from django.contrib.postgres.fields.ranges import DateRangeField
from django.core.exceptions import ValidationError
from django.core.files.images import ImageFile
from django.core.validators import MinValueValidator, RegexValidator
from django.db import models
from django.db.models.fields.files import FileDescriptor
from django.db.models.functions import Coalesce
from django.templatetags.static import static
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from closuretree.models import ClosureModel
from tinymce import HTMLField

from PIL import Image
from psycopg2.extras import DateRange

from menus.models import MenuItem
from utils.models import HierarchicalQuerySet
from utils.storages import RemoteStorage


class Category(ClosureModel):
    slug = models.SlugField(
        'Код страницы', unique=True, db_index=True,
        help_text='Латинские буквы, цифры, дефисы и нижние подчёркивания'
    )
    name = models.CharField('Имя категории', max_length=200)
    text_content = HTMLField('Текст страницы', blank=True)
    ware_default_text = HTMLField('Текст товаров', blank=True,
                                  help_text='Описание товаров по умолчанию')
    title = models.CharField('Заголовок', max_length=200,
                             help_text='Отображается в заголовке окна браузера')
    keywords = models.CharField('Ключевые слова', max_length=200, blank=True)
    description = models.TextField('Описание', blank=True)
    visible = models.BooleanField('Категория доступна пользователям',
                                  default=False, db_index=True)
    parent = models.ForeignKey('self', related_name='children', null=True,
                               verbose_name='Родительская категория')
    menu_items = GenericRelation(MenuItem, related_query_name='categories')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('catalog:detail', kwargs={'slug': self.slug})
    get_absolute_url.short_description = 'Ссылка категории'

    @property
    def menu_link(self):
        return self.get_absolute_url() if self.slug else ''

    def depth_padded_name(self):
        depth = int(len(self.path)/2 - 1)
        pad_string = mark_safe('&emsp;'*depth)
        html = '<span>{}{}</span>'
        return format_html(html, pad_string, self.name)
    depth_padded_name.admin_order_field = 'name'
    depth_padded_name.short_description = name.verbose_name

    def site_link(self):
        html = '<a target="_blank" href="{}">Ссылка</a>'
        return format_html(html, self.get_absolute_url())
    site_link.short_description = 'Ссылка на сайте'

    def is_visible(self):
        branch = self.get_ancestors(include_self=True)
        return branch.aggregate(is_visible=BoolAnd('visible'))['is_visible']

    def full_name(self):
        drill_down = self.get_ancestors(include_self=True).reverse()
        return mark_safe(' &mdash; '.join([x.name for x in drill_down]))

    objects = models.Manager()
    hierarchy = HierarchicalQuerySet.as_manager()

    @classmethod
    def get_choices_list(cls):
        """
        Returns iterable suitable for form
        fields with 'choices' attribute.
        """
        return ((x.pk, x.depth_padded_name) for x in cls.hierarchy.all())

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'
        ordering = ['name']


class WareProperty(models.Model):
    name = models.CharField('Имя', max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'свойство товара'
        verbose_name_plural = 'свойства товара'
        ordering = ['name']


class WareQuerySet(models.QuerySet):
    def active_discount(self):
        """ Prefetches active discount for every ware in qs. """
        return self.prefetch_related(
            models.Prefetch('discounts', to_attr='discount_cache',
            queryset=WareDiscount.objects.active())
        )

    def category_slug(self):
        """
        Adds slug of parent category to reduce amount of
        database queries when using get_absolute_url.
        """
        return self.annotate(category_slug=models.F('category__slug'))

    def main_image(self, size_type):
        # Using subquery to keep wares with no images in
        # queryset. Ideally it should be left outer join,
        # but django does not allow selecting type of join.
        subqs = WareThumbnail.objects.filter(
            original_image__main_image=True, size_type=size_type,
            original_image__ware_id=models.OuterRef('pk'))
        file_col = Coalesce(models.Subquery(subqs.values('file')[:1]),
                            models.Value(WareImage.DUMMY))
        return self.annotate(main_image=file_col)

    def ware_user_list(self, filters):
        """ Returns queryset for templates displaying lists of wares. """
        qs = self.filter(**filters).category_slug()
        return qs.main_image(WareThumbnail.SMALL).active_discount()


class Ware(models.Model):
    METERS = 1
    UNITS = 2
    MEASURE = (
        (METERS, 'Метры'),
        (UNITS, 'Штуки'),
    )

    category = models.ManyToManyField(
        Category, related_name='wares', related_query_name='ware',
        verbose_name='Категории товара')
    main_category = models.ForeignKey(Category, related_name='+',
                                      verbose_name='Главная категория')
    ware_properties = models.ManyToManyField(
        WareProperty, through='WarePropertyValue', related_name='wares',
        related_query_name='ware', verbose_name='Свойства товара')
    name = models.CharField('Имя', max_length=200, db_index=True)
    units = models.SmallIntegerField('Единица измерения', choices=MEASURE)
    price = models.DecimalField('Цена', max_digits=9, decimal_places=2,
                                default=0, validators=[MinValueValidator(0)])
    product_code_prefix = models.CharField(
        'Префикс артикула', max_length=10, blank=True, validators=[
            RegexValidator(regex=r'^[A-ZА-Яa-zа-яёЁ]+$', message='Только буквы',
                           code='illegal_characters')]
    )
    new_until = models.DateField(
        'Новинка до', blank=True, null=True, db_index=True)
    text_content = HTMLField('Описание', blank=True)
    title = models.CharField('Заголовок', max_length=200,
                             help_text='Отображается в заголовке окна браузера')
    keywords = models.CharField('Ключевые слова', max_length=200, blank=True)
    description = models.TextField('Описание', blank=True)
    visible = models.BooleanField('Доступен', default=True, db_index=True)

    main_image = FileDescriptor(models.ImageField(name='main_image'))

    objects = WareQuerySet.as_manager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        if hasattr(self, 'category_slug'):
            category_slug = self.category_slug
        else:
            category_slug = self.category.first().slug
        return reverse('catalog:ware_detail',
                       kwargs={'pk': self.pk, 'category_slug': category_slug})
    get_absolute_url.short_description = 'Ссылка на товар'

    def is_visible(self):
        parents_visible = any([x.is_visible for x in self.category.all()])
        return self.visible and parents_visible

    def is_new(self):
        if self.new_until is None:
            return False
        return self.new_until > datetime.date.today()

    @property
    def main_image_url(self):
        if self.main_image.url.endswith(WareImage.DUMMY):
            return static(WareImage.DUMMY)
        else:
            return self.main_image.url

    @property
    def product_code(self):
        return self.product_code_prefix + str(self.pk)

    def get_effective_description(self):
        if self.text_content:
            return self.text_content
        else:
            return self.main_category.ware_default_text

    def active_discount(self):
        if hasattr(self, 'discount_cache'):
            return self.discount_cache[0] if self.discount_cache else None
        else:
            return self.discounts.active().first()

    def derived_product_code(self):
        return self.product_code
    derived_product_code.short_description = 'Артикул'
    derived_product_code.admin_order_field = 'pk'

    class Meta:
        verbose_name = 'товар'
        verbose_name_plural = 'товары'
        ordering = ['name']


class WarePropertyValue(models.Model):
    ware = models.ForeignKey(Ware, on_delete=models.CASCADE,
                             related_name='properties', verbose_name='Товар')
    property = models.ForeignKey(WareProperty, on_delete=models.CASCADE,
                                 verbose_name='Свойство')
    value = models.CharField('Значение', max_length=100)
    sort = models.PositiveSmallIntegerField(
        'Сортировка', default=1000, db_index=True,
        help_text='От 0 до 32767. Сортировка по убыванию. '
        'Одинаковые значения сортируются по алфавиту.')

    @classmethod
    def get_category_properties(cls, category_pks):
        """ Returns property pks used by wares in specified categories. """
        filters = {'ware__category__pk__in': category_pks}
        used_properties = cls.objects.distinct(
            'property').filter(**filters).order_by()
        return [{'property': x.property_id} for x in used_properties]

    class Meta:
        verbose_name = 'значение свойства товара'
        verbose_name_plural = 'значения свойств товара'
        ordering = ['-sort', 'property__name']
        unique_together = ('ware', 'property')


class ImageThumbnailer(object):
    def __init__(self, original_image):
        self._original_image = original_image

    def _write_resized_copy_to(self, thumb_path, dimensions):
        original_image = Image.open(self._original_image)
        original_image.thumbnail(dimensions)
        original_image.save(thumb_path)
        original_image.close()

    def get_thumbnail(self, dimensions):
        _, ext = os.path.splitext(self._original_image.name)
        tmp_image = NamedTemporaryFile(suffix=ext)
        self._write_resized_copy_to(tmp_image.name, dimensions)
        return tmp_image


def get_ware_image_upload_path(instance, filename):
    hash_obj = md5()
    hash_obj.update(str(time()).encode('utf-8'))
    hashed_time = hash_obj.hexdigest()
    depth = instance.STORAGE_DIR_DEPTH
    upload_dir = os.path.join(instance.STORAGE_DIR_PREFIX,
                              *hashed_time[0:depth])
    _, ext = os.path.splitext(filename)
    return os.path.join(upload_dir, ''.join([hashed_time, ext]))


_image_storage = RemoteStorage(force_names=True)

class WareImage(models.Model):
    STORAGE_DIR_DEPTH = 2
    STORAGE_DIR_PREFIX = 'wares'

    DUMMY = 'utils/images/no-image.png'

    ware = models.ForeignKey(
        Ware, on_delete=models.CASCADE, verbose_name='Товар',
        related_name='ware_images', related_query_name='ware_image')
    file = models.ImageField('Файл', storage=_image_storage,
                             upload_to=get_ware_image_upload_path)
    main_image = models.BooleanField('Главное изображение',
                                     default=False, db_index=True)

    def __str__(self):
        return 'ware {}, image {}, original'.format(self.ware_id, self.pk)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._old_path = self.file.name if self.pk else None

    def save(self, *args, **kwargs):
        if self.main_image:
            self._reset_wares_main_image()
        is_new_model = self._state.adding
        raw_file = self.file.file
        super().save(*args, **kwargs)
        if self._old_path != self.file.name:
            WareThumbnail.objects.make_new_set(self, raw_file, is_new_model)
            if not is_new_model:
                self._remove_orphaned_image()
        self._old_path = self.file.url

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        if self.main_image:
            new_main = self.ware.ware_images.first()
            if new_main:
                new_main.main_image = True
                new_main.save()

    def _remove_orphaned_image(self):
        _image_storage.delete(self._old_path)

    def _reset_wares_main_image(self):
        self.ware.ware_images.update(main_image=False)

    def remove_image_file(self):
        self.file.delete(save=False)

    def get_absolute_url(self):
        return self.file.url

    class Meta:
        verbose_name = 'изображение товара'
        verbose_name_plural = 'изображения товара'


class WareThumbnailManager(models.Manager):
    def make_new_set(self, image_model, raw_file, image_is_new):
        if not image_is_new:
            image_model.thumbnails.all().delete()
        thumbnailer = ImageThumbnailer(raw_file)
        for size_type, dimensions in self.model.get_file_sizes().items():
            thumbnail = thumbnailer.get_thumbnail(dimensions)
            self.create(original_image=image_model, file=ImageFile(thumbnail),
                        size_type=size_type)


class WareThumbnail(models.Model):
    STORAGE_DIR_DEPTH = 2
    STORAGE_DIR_PREFIX = 'wares'

    BIG = 2
    MEDIUM = 3
    SMALL = 4
    EXTRA_SMALL = 5
    TINY = 6

    original_image = models.ForeignKey(WareImage, on_delete=models.CASCADE,
        related_name='thumbnails', related_query_name='thumbnail')
    file = models.ImageField(upload_to=get_ware_image_upload_path)
    size_type = models.SmallIntegerField(db_index=True)

    objects = WareThumbnailManager()

    def __str__(self):
        info = 'image {}, thumbnail - size type {}'
        return info.format(self.original_image_id, self.size_type)

    @classmethod
    def get_file_sizes(cls):
        return {
            cls.BIG: (1024, 1024),
            cls.MEDIUM: (512, 512),
            cls.SMALL: (256, 256),
            cls.EXTRA_SMALL: (128, 128),
            cls.TINY: (64, 64),
        }

    @classmethod
    def get_optimal_size_type(cls, min_side_size):
        file_sizes = list(cls.get_file_sizes().items())
        sorted_sizes = sorted(file_sizes, key=lambda x: x[1][0])
        for size_type, dimensions in sorted_sizes:
            if dimensions[0] > min_side_size:
                return size_type

    def remove_image_file(self):
        self.file.delete(save=False)

    def get_absolute_url(self):
        return self.file.url

    class Meta:
        unique_together = ('original_image', 'size_type')


class SaleQuerySet(models.QuerySet):
    @property
    def _today_range(self):
        today = datetime.date.today()
        return DateRange(today, today, bounds='[]')

    def active(self):
        """ Sales that have started but have not finished yet. """
        return self.filter(timeline__overlap=self._today_range)

    def future(self):
        """ Sales that will start in the future. """
        return self.filter(timeline__fully_gt=self._today_range)

    def finished(self):
        """ Sales that have finished. """
        return self.filter(timeline__fully_lt=self._today_range)

    def unfinished(self):
        """ Either future or active sales. """
        return self.exclude(timeline__fully_lt=self._today_range)


class Sale(models.Model):
    name = models.CharField('Имя', max_length=200)
    text_content = HTMLField('Описание', blank=True)
    timeline = DateRangeField(
        'Сроки', db_index=True,
        help_text='Даты начала и конца. Формат: ДД.ММ.ГГГГ')
    wares = models.ManyToManyField(
        Ware, through='WareDiscount', related_name='sales',
        related_query_name='sale', verbose_name='Товары')
    title = models.CharField('Заголовок', max_length=200,
                             help_text='Отображается в заголовке окна браузера')
    keywords = models.CharField('Ключевые слова', max_length=200, blank=True)
    description = models.TextField('Описание', blank=True)

    objects = SaleQuerySet.as_manager()

    date_format = '%d.%m.%Y'

    def __str__(self):
        return self.name

    def clean(self, *args, **kwargs):
        super().clean()
        error_dict = self._validate_non_overlapping_timeline()
        if error_dict:
            raise ValidationError(error_dict)

    def _validate_non_overlapping_timeline(self):
        if self._state.adding:
            return {}
        affected_wares = WareDiscount.objects.filter(sale=self, timeline=None)
        affected_wares = models.Subquery(affected_wares.values('ware_id'))
        qs = WareDiscount.objects.filter(
            timeline__overlap=self.timeline,
            ware_id__in=affected_wares
        )
        if qs.exists():
            ware_ids_list = qs.values_list('ware_id', flat=True)
            ware_ids = ','.join(str(x) for x in ware_ids_list)
            msg = 'Изменение сроков распродажи приведёт к частичному'\
                ' совпадению сроков для уже существующих скидок. ID: {}'
            msg = msg.format(ware_ids)
            return {'timeline': ValidationError(msg, code='timeline_overlap')}
        return {}

    def get_absolute_url(self):
        return reverse('catalog:sale_detail', kwargs={'pk': self.pk})
    get_absolute_url.short_description = 'Ссылка распродажи'

    class Meta:
        verbose_name = 'распродажа'
        verbose_name_plural = 'распродажи'
        ordering = ['name']


class WareDiscountQuerySet(models.QuerySet):
    @property
    def _today_range(self):
        today = datetime.date.today()
        return DateRange(today, today, bounds='[]')

    def effective_timeline(self):
        effective_timeline = Coalesce('timeline', 'sale__timeline')
        return self.annotate(e_timeline=effective_timeline)

    def active(self):
        return self.effective_timeline().filter(
            e_timeline__overlap=self._today_range)


class WareDiscount(models.Model):
    PERCENTAGE = 1
    NEW_PRICE = 2
    AMOUNT_CONDITION = 3
    AMOUNT_CONDITION_GIFT = 4
    MONEY_CONDITION_GIFT = 5
    DISCOUNT_TYPES = (
        (PERCENTAGE, 'Проценты'),
        (NEW_PRICE, 'Новая цена'),
        (AMOUNT_CONDITION, 'X единиц по цене Y единиц'),
        (AMOUNT_CONDITION_GIFT, 'Подарок за X единиц товара'),
        (MONEY_CONDITION_GIFT, 'Подарок за покупку товара на сумму'),
    )
    ware = models.ForeignKey(
        Ware, on_delete=models.CASCADE, verbose_name='Товар',
        related_name='discounts', related_query_name='discount')
    sale = models.ForeignKey(
        Sale, on_delete=models.CASCADE, related_name='ware_discounts',
        related_query_name='ware_discount', verbose_name='Распродажа',
        null=True, blank=True
    )
    timeline = DateRangeField(
        'Сроки', db_index=True, null=True, blank=True,
        help_text='Даты начала и конца. Формат: ДД.ММ.ГГГГ'
    )
    discount_value = models.PositiveIntegerField('Значение')
    discount_condition = models.PositiveIntegerField(
        'Условие', blank=True, default=0)
    discount_type = models.PositiveSmallIntegerField(
        'Тип скидки', choices=DISCOUNT_TYPES)

    objects = WareDiscountQuerySet.as_manager()

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs);
        error_dict = self._validate_non_overlapping_timeline()
        # If certain values are invalid further validation makes no sense
        if self._has_discount_errors():
            return
        # Replacing None with value that is safe for math operations
        if self.discount_condition is None:
            self.discount_condition = 0
        discount_type_validators = {
            self.PERCENTAGE: self._validate_percentage,
            self.NEW_PRICE: self._validate_new_price,
            self.AMOUNT_CONDITION: self._validate_amount_condition,
            self.AMOUNT_CONDITION_GIFT: self._validate_condition_gift,
            self.MONEY_CONDITION_GIFT: self._validate_condition_gift,
        }
        error_dict.update(discount_type_validators[self.discount_type]())
        if error_dict:
            raise ValidationError(error_dict)

    def _has_discount_errors(self):
        try:
            self.clean_fields()
        except ValidationError as e:
            discount_fields = ['discount_type', 'discount_value',
                               'discount_condition']
            if any(x in e.error_dict.keys() for x in discount_fields):
                return True
        return False

    def _validate_percentage(self):
        return self._validate_value_in_range(
            100, 'Скидка должна быть больше 0 и меньше 100.',
            'percent_out_of_range'
        )

    def _validate_new_price(self):
        return self._validate_value_in_range(
            self.ware.price, 'Новая цена должна быть больше 0 и меньше старой.',
            'new_price_out_of_range'
        )

    def _validate_amount_condition(self):
        msg = 'Число товара определяющее новую цену должно быть '\
            'меньше числа активирующего скидку.'
        return self._validate_value_in_range(
            self.discount_condition, msg, 'price_source_too_big',
            condition_required=True
        )

    def _validate_condition_gift(self):
        error_dict = self._validate_condition(condition_required=True)
        if not Ware.objects.filter(pk=self.discount_value).exists():
            error = ValidationError('Товара с таким номером не существует',
                                    code='gift_does_not_exist')
            error_dict.update({'discount_value': error})
        return error_dict

    def _validate_value_in_range(self, max_value, msg, code,
                                 condition_required=False):
        error_dict = self._validate_condition(
            condition_required=condition_required)
        if not 0 < self.discount_value < max_value:
            error = ValidationError(msg, code=code)
            error_dict.update({'discount_value': error})
        return error_dict

    def _validate_condition(self, condition_required):
        default_condition = self._meta.get_field(
            'discount_condition').default
        has_condition = self.discount_condition != default_condition
        for type_id, type_name in self.DISCOUNT_TYPES:
            if type_id == self.discount_type:
                break
        if has_condition and not condition_required:
            msg = 'Для типа скидки "{}" условие не предусмотренно.'.format(
                type_name)
            code = 'condition_not_needed'
        elif not has_condition and condition_required:
            msg = 'Для типа скидки "{}" условие обязательно.'.format(
                type_name)
            code = 'condition_needed'
        else:
            msg = ''
        if msg:
            return {'discount_condition': ValidationError(msg, code=code)}
        else:
            return {}

    def _validate_non_overlapping_timeline(self):
        discount_timeline = self.timeline or (
            self.sale.timeline if self.sale else None)
        # No limit to discounts without sale and timeline
        if discount_timeline is None:
            return {}
        qs = self.__class__.objects.effective_timeline()
        qs = qs.filter(e_timeline__overlap=discount_timeline,
                       ware_id=self.ware_id).exclude(pk=self.pk)
        if qs.exists():
            msg = 'Сроки частично совпадают со сроками'\
                ' уже существующей скидки.'
            return {'timeline': ValidationError(msg, code='timeline_overlap')}
        return {}

    def sale_timeline(self):
        return '{} - {}'.format(str(self.sale.timeline.lower), str(self.sale.timeline.upper))
    sale_timeline.short_description = 'Сроки распродажи'

    class Meta:
        verbose_name = 'скидка товара'
        verbose_name_plural = 'скидки товара'
        index_together = ['sale', 'ware']
