import csv
from decimal import Decimal, ROUND_HALF_UP

from django.contrib.postgres.aggregates.general import StringAgg
from django.db.models.expressions import OuterRef, Subquery, Value
from django.db.models.functions import Concat

from .models import Ware, WareProperty


class CashDeskExporter(object):
    def export_query_csv(self, query, file_object):
        query = self._add_main_category(query)
        query = self._add_properties(query)
        fieldnames = ('Наименование', 'Артикул', 'В продаже', 'Себестоимость',
                      'Цена', 'Группа', 'Описание', 'Ед.изм.')
        writer = csv.DictWriter(file_object, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(self._make_row(x) for x in query)
        return file_object

    def _add_main_category(self, query):
        return query.select_related('main_category')

    def _add_properties(self, query):
        property_and_value = Concat('properties__property__name',
                                    Value(': '), 'properties__value')
        all_properties = StringAgg(property_and_value, '; ')
        new_template = '%(function)s(%(distinct)s%(expressions)s ORDER BY {})'
        prop_name_column = '{}.name'.format(WareProperty._meta.db_table)
        all_properties.template = new_template.format(prop_name_column)
        return query.annotate(all_properties=all_properties)

    def _make_row(self, model):
        units_export_names = {
            Ware.METERS: 'м',
            Ware.UNITS: 'шт',
        }
        row = {
            'Наименование': ' '.join([model.product_code, model.name]),
            'Артикул': model.product_code,
            'В продаже': int(model.visible),
            'Себестоимость': self._calculate_base_price(model),
            'Цена': model.price,
            'Группа': model.main_category.name.upper(),
            'Описание': model.all_properties.upper(),
            'Ед.изм.': units_export_names[model.units],
        }
        return row

    def _calculate_base_price(self, model):
        # 2.8 3.3 and 65 are magic numbers
        ruble_priced_categories = [19, 79, 87]
        if model.main_category_id in ruble_priced_categories:
            base_price = model.price/Decimal(2.8)
        else:
            dollar_rate = 76
            base_price = model.price/Decimal(3.3)/65 * dollar_rate
        return base_price.quantize(Decimal('1.'), rounding=ROUND_HALF_UP)
