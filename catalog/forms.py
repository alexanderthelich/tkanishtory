import os.path

from django.conf import settings
from django.contrib.admin import TabularInline
from django.core.exceptions import ValidationError
from django.db.models.expressions import F
from django.forms import Form
from django.forms.fields import TypedChoiceField
from django.forms.models import (BaseInlineFormSet, ModelChoiceField,
                                 ModelForm, ModelMultipleChoiceField)
from django.forms.widgets import (
    HiddenInput, Input, Select, SelectMultiple)
from django.utils.safestring import mark_safe

from utils.forms import ReadOnlyField, TreeModelChoiceIterator

from .models import (Category, Sale, WareDiscount, WareImage,
                     WareThumbnail, WarePropertyValue)


class CategoryInline(TabularInline):
    classes = ('collapse',)
    exclude = ('text_content', 'ware_default_text', 'keywords', 'description')
    extra = 1
    model = Category


class WareCategoryChoiceField(ModelMultipleChoiceField):
    iterator = TreeModelChoiceIterator

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        model = self.queryset.model
        self.queryset = model.hierarchy

    def label_from_instance(self, obj):
        return obj.depth_padded_name


class WareMainCategoryChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.full_name()


class WareAdminModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._limit_main_category()

    def _limit_main_category(self):
        field = self.fields['main_category']
        field_qs = field.queryset
        if self.instance.pk is None and not self.is_bound:
            # If form is empty then don't populate the field
            valid_categories = field_qs.none()
        elif self.instance.pk and not self.is_bound:
            valid_categories = field_qs.filter(ware__pk=self.instance.pk)
        else:
            # When form is saved it is category
            # select field that holds valid ids
            self.full_clean()
            valid_categories = field_qs.filter(
                pk__in=self.cleaned_data['category'])
        field.queryset = valid_categories

    class Meta:
        field_classes = {
            'category': WareCategoryChoiceField,
            'main_category': WareMainCategoryChoiceField,
        }
        widgets = {
            'category': SelectMultiple(
                attrs={'style': 'width: 300px; height: 400px;'}),
        }

    class Media:
        js = ('admin/catalog/ware/main_category.js',)


class WareImageInlineFormSet(BaseInlineFormSet):
    def clean(self):
        """ At most one main image. """
        super().clean()
        if any(self.errors):
            return
        main_count =  sum(x.cleaned_data.get('main_image', False)
                          for x in self.forms if x not in self.deleted_forms)
        if main_count > 1:
            raise ValidationError(
                'У товара может быть только одно главное изображение',
                code='duplicate_main_image'
            )
        elif self.forms and not main_count:
            # Don't bother user with error message about
            # missing main image - we set it ourselves.
            form = [x for x in self.forms if x not in self.deleted_forms][0]
            if form.has_changed():
                form.instance.main_image = True
                form.changed_data.append('main_image')


class WareImageInline(TabularInline):
    classes = ('collapse',)
    fields = ('file', 'current_image', 'main_image')
    extra = 1
    model = WareImage
    formset = WareImageInlineFormSet
    readonly_fields = ('current_image',)

    def current_image(self, instance):
        thumb = WareThumbnail.objects.get(size_type=WareThumbnail.MEDIUM,
                                          original_image=instance)
        template = '<img src="{}" style="max-width: 386px; max-height: 386px;"/>'
        image_tag = template.format(thumb.get_absolute_url())
        return mark_safe(image_tag)
    current_image.short_description = 'Изображение'


class WarePropertyValueInline(TabularInline):
    classes = ('collapse',)
    extra = 1
    model = WarePropertyValue


class SaleWareDiscountModelForm(ModelForm):
    sale = ModelChoiceField(Sale.objects.unfinished(), required=False,
                            label='Распродажа')
    ware_name = ReadOnlyField(label='Товар', required=False)
    ware_img = ReadOnlyField(label='Изображение', required=False)

    field_order = ['sale', 'ware_name', 'ware_img', 'discount_type',
                  'discount_value', 'discount_condition', 'timeline', 'ware']

    class Media:
        css = {
            'screen': ('admin/catalog/ware/sale_ware_discount_form.css',)
        }

    class Meta:
        exclude = ('ware_name', 'ware_img')
        fields = ('sale', 'ware_name', 'ware_img', 'discount_type', 'discount_value',
                  'discount_condition', 'timeline', 'ware')
        model = WareDiscount
        widgets = {
            'ware': HiddenInput,
            'discount_type': Select(attrs={'required': True}),
            'discount_value': Input(attrs={'type': 'number', 'min': 0,
                                           'required': True}),
        }


class SaleWareDiscountInline(TabularInline):
    classes = ('collapse',)
    model = WareDiscount
    extra = 0
    max_num = 0
    fields = ('ware_image', 'ware', 'ware_id', 'discount_type',
              'discount_value', 'discount_condition', 'timeline')
    readonly_fields = ('ware_image', 'ware', 'ware_id')

    def get_queryset(self, request):
        qs = super().get_queryset(request).select_related('ware')
        qs = qs.filter(ware__ware_image__size_type=WareImage.EXTRA_SMALL,
                       ware__ware_image__main_image=True)
        return qs.annotate(main_image_path=F('ware__ware_image__file'))

    def ware_image(self, obj):
        if obj.main_image_path:
            image_path = os.path.join(settings.MEDIA_URL, obj.main_image_path)
            image_tag = '<img src="{}" />'
            return mark_safe(image_tag.format(image_path))
        else:
            return ''
    ware_image.short_description = 'Изображение'


class WareDiscountInline(TabularInline):
    classes = ('collapse',)
    model = WareDiscount
    extra = 1
    fields = ('sale', 'sale_timeline', 'discount_type', 'discount_value',
              'discount_condition', 'timeline')
    readonly_fields = ('sale_timeline',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'sale':
            kwargs['queryset'] = Sale.objects.unfinished()
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class ChangeWareCategoryForm(Form):
    category = TypedChoiceField(
        choices=Category.get_choices_list, coerce=int)
