import datetime
from hashlib import md5
import os
import tempfile
from shutil import rmtree
from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.core.files.images import ImageFile
from django.test import TestCase, SimpleTestCase, override_settings
from django.urls import reverse

from psycopg2.extras import DateRange

from .models import *


class CategoryTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.model = Category
        categories = [
            {'slug': 'cat-1', 'name': 'Category-1', 'parent': None},
            {'slug': 'cat-1-1', 'name': 'Category-1-1', 'parent': 'Category-1'},
            {'slug': 'cat-1-2', 'name': 'Category-1-2', 'parent': 'Category-1'},
            {'slug': 'cat-2', 'name': 'Category-2', 'parent': None},
            {'slug': 'cat-3', 'name': 'Category-3', 'parent': None},
            {'slug': 'cat-3-1', 'name': 'Category-3-1', 'parent': 'Category-3'},
            {'slug': 'cat-3-2', 'name': 'Category-3-2', 'parent': 'Category-3'},
            {'slug': 'cat-4', 'name': 'Category-4', 'parent': None},
        ]
        for category in categories:
            parent = category['parent']
            if parent is not None:
                parent = cls.model.objects.filter(name=parent).get()
            cls.model.objects.create(
                parent=parent, visible=True, slug=category['slug'],
                name=category['name'], title=category['name']
            )

    def _check_branch(self, category, fail=False):
        branch = self.model.objects.get(slug=category)
        if fail:
            self.assertFalse(branch.is_visible())
        else:
            self.assertTrue(branch.is_visible())

    def test_visible_without_branch(self):
        """
        Category without parents or children
        is visible if visible is True
        """
        self._check_branch('cat-2')

    def test_visible_with_branch(self):
        """
        Category without parents but with children
        is visible if visible is True
        """
        self._check_branch('cat-1')

    def test_visible_as_branch(self):
        """
        Category with parents but without children is visible
        if its visible and visible of all ancestors is True
        """
        self._check_branch('cat-1-2')

    def test_visible_with_hidden_branch(self):
        """
        Category without parents but with hidden
        children is still visible if its visible is True
        """
        category = 'cat-3'
        branch_root = self.model.objects.get(slug=category)
        branch_root.get_children().update(visible=False)
        self._check_branch(category)

    def test_hidden_without_branch(self):
        """
        Category without parents or children
        is not visible if its visible is False.
        """
        category = 'cat-4'
        self.model.objects.filter(slug=category).update(visible=False)
        self._check_branch(category, fail=True)

    def test_hidden_with_visible_ancestors(self):
        """
        Category with parents but without children
        is not visible its visible is False.
        """
        category = 'cat-3-2'
        self.model.objects.filter(slug=category).update(visible=False)
        self._check_branch(category, fail=True)

    def test_hidden_as_branch(self):
        """
        Category with parents but without children is not
        visible if one of ancestors has its visible == False.
        """
        category = 'cat-3-2'
        leaf = self.model.objects.get(slug=category)
        self.model.objects.filter(pk=leaf.parent.pk).update(visible=False)
        self._check_branch(category, fail=True)

    def test_visible_with_other_branch_hidden(self):
        """
        Category with parents but without children
        is visible if its visible is True and some
        other category has its visible == False.
        """
        self.model.objects.filter(slug='cat-1').update(visible=False)
        self._check_branch('cat-3-2', fail=False)

    def test_hidden_with_other_branch_hidden(self):
        """
        Category with parents but without children
        is not visible if its visible is False and
        some other category has its visible == False.
        """
        self.model.objects.filter(
            slug__in=['cat-1', 'cat-3']).update(visible=False)
        self._check_branch('cat-3-2', fail=True)


class CategoryDetailViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.model = Category
        categories = [
            {'slug': 'cat-1', 'name': 'Category-1', 'parent': None},
            {'slug': 'cat-1-1', 'name': 'Category-1-1', 'parent': 'Category-1'},
            {'slug': 'cat-1-2', 'name': 'Category-1-2', 'parent': 'Category-1'},
            {'slug': 'cat-2', 'name': 'Category-2', 'parent': None},
            {'slug': 'cat-3', 'name': 'Category-3', 'parent': None},
            {'slug': 'cat-3-1', 'name': 'Category-3-1', 'parent': 'Category-3'},
            {'slug': 'cat-3-2', 'name': 'Category-3-2', 'parent': 'Category-3'},
            {'slug': 'cat-4', 'name': 'Category-4', 'parent': None},
        ]
        for category in categories:
            parent = category['parent']
            if parent is not None:
                parent = cls.model.objects.filter(name=parent).get()
            cls.model.objects.create(
                parent=parent, visible=True, slug=category['slug'],
                name=category['name'], title=category['name']
            )

    def tearDown(self):
        self.model.objects.update(visible=True)

    def _check_branch(self, category, fail=False):
        response = self.client.get(reverse('catalog:detail',
                                           kwargs={'slug': category}))
        if fail:
            self.assertEqual(response.status_code, 404)
        else:
            obj_slug = response.context_data['object'].slug
            self.assertEqual(response.status_code, 200)
            self.assertEqual(obj_slug, category)

    def test_visible_without_branch(self):
        """
        Category without parents or children
        is retrieved if it is visible.
        """
        self._check_branch('cat-2')

    def test_visible_with_branch(self):
        """
        Category without parents but with
        children is retrieved if it is visible.
        """
        self._check_branch('cat-1-2')

    def test_visible_as_branch(self):
        """
        Category with parents but without
        children is retrieved if it is visible.
        """
        self._check_branch('cat-1')

    def test_hidden_without_branch(self):
        """
        Category without parents or children
        is not retrieved if it is hidden.
        """
        category = 'cat-4'
        self.model.objects.filter(slug=category).update(visible=False)
        self._check_branch(category, fail=True)

    def test_hidden_with_branch(self):
        """
        Category without parents but with children
        is not retrieved if it is hidden.
        """
        category = 'cat-3'
        self.model.objects.filter(slug=category).update(visible=False)
        self._check_branch(category, fail=True)

    def test_hidden_as_branch(self):
        """
        Category with parents but without children
        is not retrieved if it is hidden.
        """
        category = 'cat-3-2'
        self.model.objects.filter(slug=category).update(visible=False)
        self._check_branch(category, fail=True)

    def test_visible_with_other_branch_hidden(self):
        """
        Category with parents but without children is retrieved
        if it is visible and some other category is hidden.
        """
        self.model.objects.filter(slug='cat-1').update(visible=False)
        self._check_branch('cat-3-2', fail=False)

    def test_hidden_with_other_branch_hidden(self):
        """
        Category with parents but without children is not retrieved
        if it is hidden and some other category is hidden.
        """
        self.model.objects.filter(
            slug__in=['cat-1', 'cat-3']).update(visible=False)
        self._check_branch('cat-3-2', fail=True)


class GetWareImageUploadPathTest(SimpleTestCase):
    test_file = 'image.jpg'
    model = WareImage()

    @property
    def path(self):
        return get_ware_image_upload_path(self.model, self.test_file)

    def test_returns_string(self):
        """Result of calculation is a string."""
        self.assertIsInstance(self.path, str)

    def test_paths_are_unique(self):
        """The function returns unique names on each invocation."""
        path_a = self.path
        path_b = self.path
        self.assertNotEqual(path_a, path_b)

    def test_valid_directory_structure(self):
        """Generated path has expected structure and elements."""
        upload_path = self.path
        dir_prefix = self.model.STORAGE_DIR_PREFIX
        self.assertIn(dir_prefix, upload_path)
        self.assertTrue(upload_path.startswith(dir_prefix))
        generated_path = upload_path.replace(dir_prefix + os.sep, '', 1)
        dirs, file_name = os.path.split(generated_path)
        dirs = dirs.split(os.sep)
        self.assertTrue(dirs == list(file_name[0:WareImage.STORAGE_DIR_DEPTH]))


class WareImageTestData(object):
    def __init__(self):
        self._test_assets_path = os.path.join('catalog', 'test_assets')
        self._test_data = self._load_test_data()

    def _load_test_data(self):
        test_data = {'hashes': {}, 'originals': {}}
        data_file = os.path.join(self._test_assets_path,
                                      'ware_image_md5.txt')
        with open(data_file) as fh:
            for line in fh:
                hash_, filename = line.split()
                name, _ = os.path.splitext(filename)
                ware, image_num, size_type_name = name.split('_')
                hash_key = (ware, int(image_num), size_type_name)
                test_data['hashes'][hash_key] = hash_
                if 'ORIGINAL' in filename:
                    original_key = (ware, int(image_num))
                    test_data['originals'][original_key] = filename
        return test_data

    def get_original_image_path(self, ware_name, image_num):
        original_name = self._test_data['originals'][
            (ware_name.lower(), image_num)]
        return os.path.join(self._test_assets_path, original_name)

    def _get_size_name(self, size_type):
        mapping = {
            0: 'ORIGINAL',
            self.BIG: 'BIG',
            self.MEDIUM: 'MEDIUM',
            self.SMALL: 'SMALL',
        }
        return mapping[size_type]

    def get_image_hash(self, ware_name, image_num, size_type):
        size_name = self._get_size_name(size_type)
        return self._test_data['hashes'][
            (ware_name.lower(), image_num, size_name)]

    BIG = 1
    MEDIUM = 2
    SMALL = 3

    @classmethod
    def file_sizes(cls):
        return {
            cls.BIG: (256, 256),
            cls.MEDIUM: (128, 128),
            cls.SMALL: (64, 64),
        }

    def get_dimensions_for_size(self, size):
        return self.file_sizes()[size]


class WarePictureTestBase(object):
    _test_data = WareImageTestData()

    def _check_image(self, ware_image, image_num, size_type=0):
        target_hash = self._test_data.get_image_hash(
            ware_image.ware.name, image_num, size_type)
        with open(ware_image.file.path, 'rb') as fh:
            self._check_for_expected_contents(fh, target_hash)

    def _check_for_expected_contents(self, file_object, target_hash):
        hash_obj = md5()
        hash_obj.update(file_object.read())
        source_hash = hash_obj.hexdigest()
        self.assertEqual(source_hash, target_hash)


class WarePictureTestDB(WarePictureTestBase):
    @classmethod
    def setUpTestData(cls):
        category = Category(slug='test', name='Test Category')
        category.save()
        Ware.objects.bulk_create([
            Ware(name='Linen', units=Ware.METERS, main_category=category),
            Ware(name='Cotton', units=Ware.METERS, main_category=category),
            Ware(name='Wool', units=Ware.METERS, main_category=category),
        ])
        # Patching thumbnail model to prevent slowdowns
        # due to creation of unrelated files.
        patcher = patch('catalog.models.WareThumbnail')
        cls._thumbnail_model = patcher.start()
        cls.addClassCleanup(patcher.stop)

    def tearDown(self):
        tmp_dir = os.path.join(
            tempfile.gettempdir(), self._model.STORAGE_DIR_PREFIX)
        rmtree(tmp_dir)

    def _add_ware_image(self, ware_name, image_num=1, main_image=True):
        ware = Ware.objects.get(name=ware_name)
        orig_file_path = self._test_data.get_original_image_path(
            ware_name, image_num)
        wi = WareImage.objects.create(ware=ware, main_image=main_image,
            file=ImageFile(open(orig_file_path, 'rb'))
        )
        return wi


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class ImageThumbnailerTest(SimpleTestCase, WarePictureTestBase):
    def _check_thumbnail(self, thumbnailer, original_key, size_type):
        dimensions = self._test_data.get_dimensions_for_size(size_type)
        thumbnail = thumbnailer.get_thumbnail(dimensions)
        target_hash = self._test_data.get_image_hash(
            *original_key, size_type)
        self._check_for_expected_contents(thumbnail, target_hash)

    def _get_thumbnailer_for_key(self, key):
        orighinal_image = self._test_data.get_original_image_path(*key)
        return ImageThumbnailer(orighinal_image)

    def test_thumbnail_is_properly_made(self):
        """ Single thumbnail has expected contents. """
        original_key = ('linen', 1)
        thumbnailer = self._get_thumbnailer_for_key(original_key)
        self._check_thumbnail(thumbnailer, original_key, self._test_data.MEDIUM)

    def test_thumbnails_are_properly_made(self):
        """ Thumbnails of different sizes have expected contents. """
        original_key = ('cotton', 1)
        thumbnailer = self._get_thumbnailer_for_key(original_key)
        self._check_thumbnail(thumbnailer, original_key, self._test_data.BIG)
        self._check_thumbnail(thumbnailer, original_key, self._test_data.SMALL)


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class WareImageTest(WarePictureTestDB, TestCase):
    _model = WareImage

    def test_file_saved(self):
        """
        After writing model to database the image is
        stored at location specified by model field.
        """
        wi = self._add_ware_image('Linen')
        self.assertTrue(os.path.isfile(wi.file.path))
        self._check_image(wi, image_num=1)

    def test_thumbnails_created(self):
        """
        Process of making thumbnail set is
        started after saving original file.
        """
        self._thumbnail_model.reset_mock()
        wi = self._add_ware_image('Linen')
        method = self._thumbnail_model.objects.make_new_set
        method.assert_called_once_with(wi, True)

    def test_deleting_instance_directly_also_deletes_file(self):
        """ Deleting model also deletes actual file on disk. """
        wi = self._add_ware_image('Linen')
        old_path = wi.file.path
        wi.delete()
        self.assertFalse(os.path.isfile(old_path))

    def test_updating_image_deletes_old_filename(self):
        """
        Changing image file deletes old one from
        disk without creating new instance.
        """
        wi = self._add_ware_image('Linen')
        old_file_path = wi.file.path
        old_pk = wi.pk
        new_image = self._test_data.get_original_image_path(wi.ware.name, 2)
        wi.file = ImageFile(open(new_image, 'rb'))
        wi.save()
        wi2 = self._model.objects.get(pk=wi.pk)
        self.assertEqual(old_pk, wi.pk)
        self.assertFalse(os.path.isfile(old_file_path))
        self._check_image(wi, 2)
        self._check_image(wi2, 2)

    def test_thumbnails_updated(self):
        """
        Process of making thumbnail set is
        started after changing original file.
        """
        wi = self._add_ware_image('Linen')
        self._thumbnail_model.reset_mock()
        other_image = self._test_data.get_original_image_path('linen', 2)
        wi.file = ImageFile(open(other_image, 'rb'))
        wi.save()
        method = self._thumbnail_model.objects.make_new_set
        method.assert_called_once_with(wi, False)

    def test_can_not_create_duplicate_main_images(self):
        """
        It is not possible to create more than one main image for the
        same ware. Adding another one simply sets new one as main.
        """
        wi = self._add_ware_image('Linen')
        self.assertTrue(wi.main_image)
        wi2 = self._add_ware_image('Linen', image_num=2)
        wi.refresh_from_db()
        self.assertFalse(wi.main_image)
        self.assertTrue(wi2.main_image)

    def test_can_not_update_to_make_duplicate_main_images(self):
        """
        It is not possible to update ware images in such a way that
        there is more than one main image for the same ware.
        """
        wi = self._add_ware_image('Linen')
        wi2 = self._add_ware_image('Linen', image_num=2, main_image=False)
        wi2.main_image = True
        wi2.save()
        wi.refresh_from_db()
        self.assertFalse(wi.main_image)
        self.assertTrue(wi2.main_image)

    def test_deleting_main_image_assigns_new_main_image(self):
        """
        Deleting image flagged as main will set this
        flag for some other image of same ware.
        """
        wi = self._add_ware_image('Linen')
        wi2 = self._add_ware_image('Linen', image_num=2, main_image=False)
        wi.delete()
        wi2.refresh_from_db()
        self.assertTrue(wi2.main_image)

    def test_changing_main_image_for_one_ware_does_not_affect_another(self):
        other_wi = self._add_ware_image('Cotton')
        self._add_ware_image('Linen')
        other_wi.refresh_from_db()
        self.assertTrue(other_wi.main_image)
        wi2 = self._add_ware_image('Linen', image_num=2, main_image=False)
        wi2.main_image = True
        wi2.save()
        other_wi.refresh_from_db()
        self.assertTrue(other_wi.main_image)
        self._add_ware_image('Linen', image_num=3)
        other_wi.refresh_from_db()
        self.assertTrue(other_wi.main_image)

    def test_duplicate_image_causes_no_error(self):
        """
        Creating new image when exact copy of it already exists does not
        cause any errors and simply adds new object with same image.
        """
        original = self._add_ware_image('Linen')
        copy = self._add_ware_image('Linen', main_image=False)
        self.assertNotEqual(original.pk, copy.pk)
        original_hash = md5()
        copy_hash = md5()
        with open(original.file.path, 'rb') as original_img:
            original_hash.update(original_img.read())
        with open(copy.file.path, 'rb') as copy_img:
            copy_hash.update(copy_img.read())
        self.assertEqual(original_hash.hexdigest(), copy_hash.hexdigest())

@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class WareImageSetLevelTest(WarePictureTestDB, TestCase):
    _model = WareImage

    def test_deleting_ware_deletes_its_ware_image_files_from_disk(self):
        """ Deleting a ware deletes all of its images from disk too. """
        wi = self._add_ware_image('Linen')
        wi2 = self._add_ware_image('Linen', image_num=2, main_image=False)
        deleted_images = [wi.file.path, wi2.file.path]
        w = Ware.objects.get(name='Linen')
        w.delete()
        self.assertFalse(self._model.objects.filter(ware_id=wi.ware_id))
        self.assertFalse(any(os.path.isfile(x) for x in deleted_images))

    def test_bulk_deleting_ware_images_deletes_their_files_from_disk(self):
        """ Using bulk deletion properly removes image files. """
        wi = self._add_ware_image('Linen')
        wi2 = self._add_ware_image('Linen', image_num=2, main_image=False)
        deleted_images = [wi.file.path, wi2.file.path]
        self._model.objects.all().delete()
        self.assertFalse(self._model.objects.all())
        self.assertFalse(any(os.path.isfile(x) for x in deleted_images))

    def test_deleting_ware_deletes_its_ware_images_but_not_other_images(self):
        """
        Deleting single ware deletes only associated images.
        Images of other wares remain unaffected.
        """
        cotton_image = self._add_ware_image('Cotton')
        self._add_ware_image('Linen')
        self._add_ware_image('Linen', image_num=2, main_image=False)
        Ware.objects.get(name='Linen').delete()
        self.assertTrue(os.path.isfile(cotton_image.file.path))
        self._check_image(cotton_image, image_num=1)


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class WareThumbnailSetLevelTest(WarePictureTestDB, TestCase):
    _model = WareThumbnail

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        patch.stopall()
        sizes_patch = patch.object(WareThumbnail, 'get_file_sizes',
                                   cls._test_data.file_sizes)
        sizes_patch.start()
        cls.addClassCleanup(sizes_patch.stop)

    def test_thumbnails_created(self):
        """ Adding original image creates a set of thumbnails. """
        wi = self._add_ware_image('Linen')
        thumbnails = wi.thumbnails.all()
        self.assertEqual(len(thumbnails), len(self._model.get_file_sizes()))
        for thumb in thumbnails:
            thumb.ware = thumb.original_image.ware
            with self.subTest(thumb=thumb):
                self.assertTrue(os.path.isfile(thumb.file.path))
                self._check_image(thumb, 1, thumb.size_type)

    def test_changing_image_deletes_old_ones(self):
        """
        Changing original image file deletes
        old original and all of its thumbnails.
        """
        wi = self._add_ware_image('Linen')
        old_thumbnails = [x.file.path for x in wi.thumbnails.all()]
        new_image = self._test_data.get_original_image_path(wi.ware.name, 2)
        wi.file = ImageFile(open(new_image, 'rb'))
        wi.save()
        self.assertFalse(any(os.path.isfile(x) for x in old_thumbnails))
        for thumb in wi.thumbnails.all():
            thumb.ware = thumb.original_image.ware
            with self.subTest(thumb=thumb):
                self._check_image(thumb, 2, thumb.size_type)

    def test_deleting_original_its_thumbnails_but_not_other_thumbnails(self):
        """
        Deleting original image deletes its thumbnails
        but not thumbnails of other images.
        """
        wi = self._add_ware_image('Linen')
        wi2 = self._add_ware_image('Linen', image_num=2, main_image=False)
        old_thumbnails = [x.file.path for x in wi.thumbnails.all()]
        wi.delete()
        self.assertFalse(any(os.path.isfile(x) for x in old_thumbnails))
        self.assertEqual(self._model.objects.count(), wi2.thumbnails.count())

    def test_deleting_ware_deletes_all_thumbnails_with_files(self):
        """
        Deleting ware removes all database thumbnail
        entries and any image files on disk.
        """
        wi = self._add_ware_image('Linen')
        old_thumbnails = [x.file.path for x in wi.thumbnails.all()]
        wi.ware.delete()
        self.assertFalse(any(os.path.isfile(x) for x in old_thumbnails))
        self.assertEqual(self._model.objects.count(), 0)


class SaleQuerySetTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        delta = datetime.timedelta
        today = datetime.date.today()
        sales_data = (
            ('Ongoing', today - delta(days=1), today + delta(weeks=4)),
            ('Planned', today + delta(weeks=1), today + delta(weeks=5)),
            ('Expired', today - delta(weeks=5), today - delta(weeks=1)),
        )
        sales = []
        for name, start, end in sales_data:
            sales.append(Sale(name=name, title=name,
                              timeline=DateRange(start, end)))
        Sale.objects.bulk_create(sales)

    def test_active(self):
        """ Only active sales are returned. """
        sample = Sale.objects.active().first()
        self.assertEqual(sample.name, 'Ongoing')

    def test_future(self):
        """ Only sales that will start in future are returned. """
        sample = Sale.objects.future().first()
        self.assertEqual(sample.name, 'Planned')

    def test_finished(self):
        """ Only finished sales are returned. """
        sample = Sale.objects.finished().first()
        self.assertEqual(sample.name, 'Expired')

    def test_unfinished(self):
        """ All finished sales are excluded. """
        names = [x.name for x in Sale.objects.unfinished()]
        self.assertNotIn('Expired', names)
        self.assertCountEqual(['Ongoing', 'Planned'], names)


def get_date_range(offset_start, offset_end):
    start = datetime.date.today() - datetime.timedelta(days=offset_start)
    end = datetime.date.today() + datetime.timedelta(days=offset_end)
    return DateRange(start, end)


class WareDiscountTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        timeline = DateRange(datetime.date.today() - datetime.timedelta(days=30),
                             datetime.date.today() + datetime.timedelta(days=30))
        category = Category.objects.create(slug='test', name='Test Category')
        cls.test_sale = Sale.objects.create(
            name='Test Sale', title='Test Sale', timeline=timeline)
        cls.discounted_ware = Ware.objects.create(
            name='Discounted ware', title='Discounted ware',
            units=Ware.UNITS, visible=True, price=250,
            main_category=category
        )
        cls.ware_discount_params = {'ware': cls.discounted_ware,
                                    'sale': cls.test_sale}
        cls.gift_ware = Ware.objects.create(
            name='Gift ware', title='Gift ware', units=Ware.UNITS,
            visible=True, main_category=category
        )

    @property
    def _types_test_data(self):
        test_ware = self.ware_discount_params['ware']
        return {
            WareDiscount.PERCENTAGE: {
                'discount_value': 20,
                'discount_condition': 0,
                'max_value': 100,
                'code_value': 'percent_out_of_range',
                'code_condition': 'condition_not_needed',
            },
            WareDiscount.NEW_PRICE: {
                'discount_value': test_ware.price - 1,
                'discount_condition': 0,
                'max_value': test_ware.price * 2,
                'code_value': 'new_price_out_of_range',
                'code_condition': 'condition_not_needed',
            },
            WareDiscount.AMOUNT_CONDITION: {
                'discount_value': 2,
                'discount_condition': 3,
                'max_value': 3,
                'code_value': 'price_source_too_big',
                'code_condition': 'condition_needed',
            },
            WareDiscount.AMOUNT_CONDITION_GIFT: {
                'discount_value': self.gift_ware.pk,
                'discount_condition': 10,
                'code_value': 'gift_does_not_exist',
                'code_condition': 'condition_needed',
            },
            WareDiscount.MONEY_CONDITION_GIFT: {
                'discount_value': self.gift_ware.pk,
                'discount_condition': 1000,
                'code_value': 'gift_does_not_exist',
                'code_condition': 'condition_needed',
            },
        }

    def _get_params_for_type(self, type_id):
        params = {'discount_type': type_id}
        params.update(self.ware_discount_params)
        return params

    def _test_error_code(self, error, field, code):
        field_error = error.exception.error_dict[field]
        self.assertEqual(len(field_error), 1) # No other errors for this field
        self.assertEqual(field_error[0].code, code)

    def _try_saving(self, params):
        discount = WareDiscount(**params)
        discount.full_clean()
        discount.save()
        discount.delete()

    def _get_test_data_for_types(self, types):
        types_data = self._types_test_data
        return {k: types_data[k] for k in types}

    def test_discounts_with_correct_params(self):
        """ All of discount types save normaly with correct data. """
        for type_id, type_data in self._types_test_data.items():
            with self.subTest(type_id=type_id):
                params = self._get_params_for_type(type_id)
                params['discount_value'] = type_data['discount_value']
                params['discount_condition'] = type_data['discount_condition']
                self._try_saving(params)

    def test_discounts_with_incorrect_conditions(self):
        """
        Adding condition to type that does not require it or not
        adding to type that does require it results in error.
        """
        for type_id, type_data in self._types_test_data.items():
            with self.subTest(type_id=type_id):
                params = self._get_params_for_type(type_id)
                params['discount_value'] = type_data['discount_value']
                params['discount_condition'] = int(
                    not type_data['discount_condition'])
                with self.assertRaises(ValidationError) as error:
                    self._try_saving(params)
                self._test_error_code(error, 'discount_condition',
                                      type_data['code_condition'])

    def test_values_in_valid_range(self):
        """ Test for discount types whose values must be in specific range. """
        types = (WareDiscount.PERCENTAGE, WareDiscount.NEW_PRICE,
                       WareDiscount.AMOUNT_CONDITION)
        for type_id, type_data in self._get_test_data_for_types(types).items():
            with self.subTest(type_id=type_id):
                basic_params = self._get_params_for_type(type_id)
                params_low = basic_params.copy()
                params_low['discount_value'] = 0
                params_high = basic_params.copy()
                params_high['discount_value'] = type_data['max_value']
                with self.assertRaises(ValidationError) as error_low:
                    self._try_saving(params_low)
                with self.assertRaises(ValidationError) as error_high:
                    self._try_saving(params_high)
                self._test_error_code(error_low, 'discount_value',
                                      type_data['code_value'])
                self._test_error_code(error_high, 'discount_value',
                                      type_data['code_value'])

    def test_values_as_ware_id(self):
        """ Test for discount types whose values are id of wares. """
        types = (WareDiscount.AMOUNT_CONDITION_GIFT,
                 WareDiscount.MONEY_CONDITION_GIFT)
        for type_id, type_data in self._get_test_data_for_types(types).items():
            with self.subTest(type_id=type_id):
                params = self._get_params_for_type(type_id)
                params['discount_value'] = 0
                params['discount_condition'] = type_data['discount_condition']
                with self.assertRaises(ValidationError) as error:
                    self._try_saving(params)
                self._test_error_code(error, 'discount_value',
                                      type_data['code_value'])

    def test_discounts_with_none_condition(self):
        """
        When discount_condition is None it behaves
        the same way as when discount_condition == 0
        """
        for type_id, type_data in self._types_test_data.items():
            with self.subTest(type_id=type_id):
                params = self._get_params_for_type(type_id)
                params['discount_value'] = type_data['discount_value']
                params['discount_condition'] = None
                if type_data['discount_condition']:
                    with self.assertRaises(ValidationError) as error:
                        self._try_saving(params)
                    self._test_error_code(error, 'discount_condition',
                                          type_data['code_condition'])

    def test_no_custom_validation_if_discount_fields_are_invalid(self):
        """
        If discount_value, discount_condition or discount_type have
        invalid values custom code in model.clean() is not used.
        """
        for val, code in ((None, 'null'), (-1, 'min_value')):
            for type_id, type_data in self._types_test_data.items():
                with self.subTest(type_id=type_id):
                    params = self._get_params_for_type(type_id)
                    params['discount_value'] = val
                    params['discount_condition'] = type_data[
                        'discount_condition']
                    with self.assertRaises(ValidationError) as error:
                        self._try_saving(params)
                    self._test_error_code(error, 'discount_value', code)

    def _check_non_overlapping_discounts(self, discounts):
        for discount in discounts:
            with self.subTest(timeline=discount.timeline):
                discount.full_clean()
                discount.save()

    def test_no_timeline_overlap_for_non_sale_wares(self):
        """
        Several discounts for the same ware that are
        not part of any sale with timelines that do
        not overlap do not raise any exceptions.
        """
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(0, 10),
                discount_value=10, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-11, 20),
                discount_value=20, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-21, 30),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE
            ),
        )
        self._check_non_overlapping_discounts(discounts)

    def test_no_timeline_overlap_for_sale_wares(self):
        """
        Several discounts for the same ware that are
        part of same sale with timelines that do
        not overlap do not raise any exceptions.
        """
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, discount_value=10,
                discount_type=WareDiscount.PERCENTAGE, sale=self.test_sale
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-30, 40),
                discount_value=20, discount_type=WareDiscount.PERCENTAGE,
                sale=self.test_sale
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-40, 50),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE,
                sale=self.test_sale
            ),
        )
        self._check_non_overlapping_discounts(discounts)

    def test_no_timeline_overlap_for_wares(self):
        """
        Several discounts for the same ware that are
        both part of some sale and not with timelines
        that do not overlap do not raise any exceptions.
        """
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(40, -30),
                discount_value=10, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=20, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-40, 50),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE
            ),
        )
        self._check_non_overlapping_discounts(discounts)

    def test_no_timeline_overlap_for_wares_without_timelines(self):
        """
        Several discounts for the same ware that are
        both part of some sale and not with timelines
        that are not specified do not raise any error.
        """
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, discount_value=10,
                discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=20, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, discount_value=30,
                discount_type=WareDiscount.PERCENTAGE
            ),
        )
        self._check_non_overlapping_discounts(discounts)

    def _test_no_timeline_overlap_with_several_empty(self):
        """
        Several discounts with no timeline for the
        same ware and sale do not raise any errors.
        """
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=10, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=20, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-40, 50),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE
            ),
        )
        self._check_non_overlapping_discounts(discounts)

    def test_no_timeline_overlap_with_self(self):
        """ Updating discount timeline does not cause any errors. """
        discounts = (
            WareDiscount(
                ware=self.gift_ware, sale=self.test_sale,
                discount_value=10, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=20, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-40, 50),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE
            ),
        )
        self._check_non_overlapping_discounts(discounts)
        discount = WareDiscount.objects.get(ware=self.discounted_ware,
                                            sale=self.test_sale)
        for start, end in ((30, 20), (30, 25)):
            discount.timeline = get_date_range(start, end)
            discount.full_clean()
            discount.save()

    def test_no_timeline_overlap_for_same_ware(self):
        """
        Adding several discounts for same ware,same sale and with
        different explicit timelines does not cause any errors.
        """
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=10, discount_type=WareDiscount.PERCENTAGE,
                timeline=get_date_range(30, -15)
            ),
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=20, discount_type=WareDiscount.PERCENTAGE,
                timeline=get_date_range(15, 0)
            ),
            WareDiscount(
                ware=self.discounted_ware, sale=self.test_sale,
                discount_value=30, discount_type=WareDiscount.PERCENTAGE,
                timeline=get_date_range(0, 15)
            ),
        )
        self._check_non_overlapping_discounts(discounts)

    def _check_overlapping_discounts(self, discounts):
        for discount in discounts:
            with self.subTest(timeline=discount.timeline):
                with self.assertRaises(ValidationError) as error:
                    discount.full_clean()
                self._test_error_code(error, 'timeline', 'timeline_overlap')

    def test_timeline_overlap_for_non_sale_wares(self):
        """
        Several discounts for the same ware that are not part of
        any sale will raise error if their timelines do overlap.
        """
        WareDiscount.objects.create(
            ware=self.discounted_ware, timeline=get_date_range(0, 10),
            discount_value=10, discount_type=WareDiscount.PERCENTAGE
        )
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-9, 20),
                discount_value=20, discount_type=WareDiscount.PERCENTAGE
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(9, 30),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE
            ),
        )
        self._check_overlapping_discounts(discounts)

    def test_timeline_overlap_for_sale_wares(self):
        """
        Several discounts for the same ware that are part of some
        sale will raise error if their timelines do overlap.
        """
        WareDiscount.objects.create(
            ware=self.discounted_ware, sale=self.test_sale,
            discount_value=10, discount_type=WareDiscount.PERCENTAGE
        )
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-9, 20),
                discount_value=20, discount_type=WareDiscount.PERCENTAGE,
                sale=self.test_sale
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(9, 30),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE,
                sale=self.test_sale
            ),
        )
        self._check_overlapping_discounts(discounts)

    def test_timeline_overlap_wares(self):
        """
        Several discounts for the same ware that are both part of some
        sale and not will raise error if their timelines do overlap.
        """
        WareDiscount.objects.create(
            ware=self.discounted_ware, sale=self.test_sale,
            discount_value=10, discount_type=WareDiscount.PERCENTAGE
        )
        WareDiscount.objects.create(
            ware=self.discounted_ware, timeline=get_date_range(-30, 40),
            discount_value=20, discount_type=WareDiscount.PERCENTAGE,
            sale=self.test_sale
        )
        discounts = (
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(-9, 20),
                discount_value=20, discount_type=WareDiscount.PERCENTAGE,
                sale=self.test_sale
            ),
            WareDiscount(
                ware=self.discounted_ware, timeline=get_date_range(9, 30),
                discount_value=30, discount_type=WareDiscount.PERCENTAGE,
                sale=self.test_sale
            ),
        )
        self._check_overlapping_discounts(discounts)

    def test_timeline_overlap_for_same_ware_with_and_without_timeline(self):
        """
        Adding several discounts with explicit timeline and
        then setting one of them to None raises exception
        if new effective timeline overlaps with old ones.
        """
        WareDiscount.objects.create(
            ware=self.discounted_ware, sale=self.test_sale,
            discount_value=10, discount_type=WareDiscount.PERCENTAGE,
            timeline=get_date_range(30, -15)
        )
        WareDiscount.objects.create(
            ware=self.discounted_ware, sale=self.test_sale,
            discount_value=20, discount_type=WareDiscount.PERCENTAGE,
            timeline=get_date_range(15, 0)
        )
        test_discount = WareDiscount.objects.create(
            ware=self.discounted_ware, sale=self.test_sale,
            discount_value=30, discount_type=WareDiscount.PERCENTAGE,
            timeline=get_date_range(0, 15)
        )
        with self.assertRaises(ValidationError) as error:
            test_discount.timeline = None
            test_discount.full_clean()
        self._test_error_code(error, 'timeline', 'timeline_overlap')


class SaleTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.main_sale = Sale.objects.create(
            name='Sale 1', title='Test Sale', timeline=get_date_range(10, 30))
        timelines = (
            ((-30, 40, None), (None, None, cls.main_sale),),
            ((10, 20, cls.main_sale),),
            ((10, 20, cls.main_sale),),
            ((None, None, cls.main_sale),),
        )
        category = Category.objects.create(slug='test', name='Test Category')
        for i, timeline_data in zip(range(len(timelines)), timelines):
            ware = Ware.objects.create(
                name='Ware'+str(i), title='Ware'+str(i),
                units=Ware.UNITS, main_category=category
            )
            for start, end, sale in timeline_data:
                date_range = None
                if start or end:
                    date_range = get_date_range(start, end)
                WareDiscount.objects.create(
                    discount_type=WareDiscount.PERCENTAGE, discount_value=100,
                    timeline=date_range, ware=ware, sale=sale
                )

    def _test_error_code(self, error, field, code):
        field_error = error.exception.error_dict[field]
        self.assertEqual(len(field_error), 1) # No other errors for this field
        self.assertEqual(field_error[0].code, code)

    def test_change_timeline_without_overlap(self):
        """ Changing sale timeline to a safe value does not cause any errors. """
        self.main_sale.timeline = get_date_range(10, 25)
        self.main_sale.full_clean()

    def test_change_timeline_with_overlap(self):
        """ Changing sale timeline to unsafe value cause expected errors. """
        self.main_sale.timeline = get_date_range(10, 35)
        with self.assertRaises(ValidationError) as error:
            self.main_sale.full_clean()
        self._test_error_code(error, 'timeline', 'timeline_overlap')

    def test_overlap_error_message_has_ware_id(self):
        """
        Changing sale timeline to unsafe value yields error
        message with id of wares which have conflicting discounts.
        """
        self.main_sale.timeline = get_date_range(10, 35)
        with self.assertRaises(ValidationError) as error:
            self.main_sale.full_clean()
        conflicting_id = Ware.objects.get(name='Ware0').pk
        msg = error.exception.error_dict['timeline'][0].message
        msg.index('ID: {}'.format(','.join([str(conflicting_id)])))
