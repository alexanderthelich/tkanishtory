from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from closuretree.models import ClosureModel

from utils.models import HierarchicalQuerySet


class MenuItemHierarchicalQuerySet(HierarchicalQuerySet):
    def visible_items(self):
        return self.filter_tree(visible=True)

    def menu_ordered(self):
        menu_ordering = self._ordering
        menu_ordering.insert(0, 'menu')
        return self.order_by(*menu_ordering)


class MenuItem(ClosureModel):
    MENU = (
        ('menu_top', 'Меню в шапке'),
        ('menu_left', 'Меню слева'),
    )

    menu = models.SlugField(max_length=32, choices=MENU, verbose_name='Меню')
    parent = models.ForeignKey('self', related_name='children', null=True,
                               verbose_name='Родительский элемент')
    url = models.CharField('Ссылка на страницу', max_length=300,
                           help_text='В конце обязательно должна быть /')
    name = models.CharField('Отображаемое имя', max_length=200)
    visible = models.BooleanField('Отображается на сайте',
                                  default=False, db_index=True)
    sort = models.PositiveSmallIntegerField(
        'Сортировка', default=1000, db_index=True,
        help_text='От 0 до 32767. Сортировка по убыванию. '
        'Одинаковые значения сортируются по алфавиту.')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey()

    def __str__(self):
        return self.name

    def depth_padded_name(self):
        depth = int(len(self.path)/2 - 1)
        pad_string = mark_safe('&emsp;'*depth)
        html = '<span>{}{}</span>'
        return format_html(html, pad_string, self.name)
    depth_padded_name.admin_order_field = 'name'
    depth_padded_name.short_description = name.verbose_name

    objects = models.Manager()
    hierarchy = MenuItemHierarchicalQuerySet.as_manager()

    class Meta:
        verbose_name = 'пункт меню'
        verbose_name_plural = 'пункты меню'
        ordering = ['-sort', 'name']
        index_together = [
            ['menu', 'sort', 'name']
        ]
