from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ValidationError
from django.test import TestCase, RequestFactory

from .models import MenuItem
from menus.context_processors import menu_tree
from utils.general import NodeTreeGenerator


MenuItem.MENU = (
    ('left', 'Left Menu'),
    ('right', 'Right Menu'),
)


class MenuTreeContextProcessorTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        factory = RequestFactory()
        cls.request = factory.get('/')
        cls.request.user = AnonymousUser()
        cls.menus = [x[0] for x in MenuItem.MENU]
        tree_gen = NodeTreeGenerator(2, 4)
        for m in cls.menus:
            sample = MenuItem(menu=m, url='/', name='Category', visible=True)
            tree_gen.generate(sample, name_attr='name')
        MenuItem.rebuildtable()

    def setUp(self):
        MenuItem.objects.update(visible=True)
        self.change_odd_roots(self.menus)

    def change_odd_roots(self, change_to):
        qs = MenuItem.objects.order_by('pk').filter(parent_id=None)
        for root, menu in zip(qs[::2], change_to):
            items = root.get_descendants(include_self=True)
            items.update(menu=menu)

    def hide_some_nodes(self):
        for menu in self.menus:
            node_to_hide = MenuItem.objects.order_by('pk').get(
                parent_id=None, menu=menu).get_children().first()
            node_to_hide.visible = False
            node_to_hide.save()

    def test_menus_are_correctly_formed(self):
        """
        With all menu items visible, checks that menu varialbles
        contain only menu items that belong to this menu.
        """
        # Trying to confuse queryset and context_processor by
        # making name and pk not dependent from their menu.
        self.change_odd_roots(self.menus[::-1])
        context = menu_tree(self.request)
        for menu in self.menus:
            stored_menu = list(MenuItem.hierarchy.filter(menu=menu))
            context_menu = context[menu]
            with self.subTest(menu=menu):
                self.assertEqual(stored_menu, context_menu)
                self.assertTrue(all([x.menu == menu for x in context_menu]))

    def test_expected_amount_of_queries_made(self):
        """menu_tree must make only one query to the database."""
        with self.assertNumQueries(1):
            context = menu_tree(self.request)

    def test_menus_are_correctly_formed_with_hidden_items(self):
        """menu_tree must ignore menu items if they are hidden"""
        self.hide_some_nodes()
        context = menu_tree(self.request)
        for menu in self.menus:
            stored_menu = MenuItem.hierarchy.filter(menu=menu).visible_items()
            self.assertEqual(list(stored_menu), context[menu])

    def test_with_all_items_hidden_empty_variable_created(self):
        """
        If menu has no items visible context variable
        is still created. It will hold empty list.
        """
        hidden_menu = self.menus[0]
        qs = MenuItem.objects.filter(parent_id=None, menu=hidden_menu)
        qs.update(visible=False)
        context = menu_tree(self.request)
        self.assertEqual(len(context[hidden_menu]), 0)


class MenuItemUniqueSiblingsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        roots = [
            {'url': '/root1', 'name': 'Root Element 1', 'parent': None},
            {'url': '/root2', 'name': 'Root Element 2', 'parent': None},
        ]
        for params in roots:
            cls._insert_item(params)

    @classmethod
    def _insert_item(cls, params):
            MenuItem.objects.create(menu='menu_left', content_type_id=14,
                                    object_id=1, **params)

    def _get_parent(self, parent_url):
        return MenuItem.objects.get(url=parent_url)

    def _insert_test_element(self):
        child = {'url': '/root2/test/', 'name': 'Element 2-0',
                 'parent': self._get_parent('/root2')}
        self._insert_item(child)
        return child

    def _check_test_element(self):
        element = self._insert_test_element()
        inserted = MenuItem.objects.filter(
            url=element['url'], parent_id=element['parent']).count()
        self.assertTrue(inserted)
        self.assertEqual(inserted, 1)

    def test_inserts_without_any_children(self):
        """
        Subitem is successfully inserted when
        there is no child elements anywhere.
        """
        self._check_test_element()

    def test_inserts_without_siblings_in_current_branch(self):
        """
        Subitem is successfully inserted without any sibings
        in current branch but with child elements in another.
        """
        root1 = self._get_parent('/root1')
        other_children = [
            {'url': '/root1/test1/', 'name': 'Element 1-1', 'parent': root1},
            {'url': '/root1/test2/', 'name': 'Element 1-2', 'parent': root1},
        ]
        for element in other_children:
            self._insert_item(element)
        self._check_test_element()

    def test_inserts_with_unique_child_elements_everywhere(self):
        """
        Subitem is successfully inserted when all
        parent elements have unique child elements.
        """
        root1 = self._get_parent('/root1')
        root2 = self._get_parent('/root2')
        other_children = [
            {'url': '/root1/test1/', 'name': 'Element 1-1', 'parent': root1},
            {'url': '/root1/test2/', 'name': 'Element 1-2', 'parent': root1},
            {'url': '/root2/test2/', 'name': 'Element 2-2', 'parent': root2},
            {'url': '/root2/test3/', 'name': 'Element 2-3', 'parent': root2},
        ]
        for element in other_children:
            self._insert_item(element)
        self._check_test_element()

    def test_inserts_with_same_url_in_another_branch(self):
        """
        Subitem is successfully inserted even when there
        is child element with same url in another branch.
        """
        root1 = self._get_parent('/root1')
        root2 = self._get_parent('/root2')
        other_children = [
            {'url': '/root2/test/', 'name': 'Element 1-0', 'parent': root1},
            {'url': '/root1/test1/', 'name': 'Element 1-1', 'parent': root1},
            {'url': '/root1/test2/', 'name': 'Element 1-2', 'parent': root1},
            {'url': '/root2/test2/', 'name': 'Element 2-2', 'parent': root2},
            {'url': '/root2/test3/', 'name': 'Element 2-3', 'parent': root2},
        ]
        for element in other_children:
            self._insert_item(element)
        self._check_test_element()

    def test_fails_to_insert_non_unique_child(self):
        """
        Subitem is not successfully inserted when there is already a
        child element with the same url belonging to the same parent.
        """
        root1 = self._get_parent('/root1')
        root2 = self._get_parent('/root2')
        other_children = [
            {'url': '/root2/test/', 'name': 'Element 1-0', 'parent': root1},
            {'url': '/root2/test/', 'name': 'Element 2-0', 'parent': root2},
            {'url': '/root1/test1/', 'name': 'Element 1-1', 'parent': root1},
            {'url': '/root1/test2/', 'name': 'Element 1-2', 'parent': root1},
            {'url': '/root2/test2/', 'name': 'Element 2-2', 'parent': root2},
            {'url': '/root2/test3/', 'name': 'Element 2-3', 'parent': root2},
        ]
        for element in other_children:
            self._insert_item(element)
        with self.assertRaises(ValidationError) as cm:
            self._check_test_element()
        self.assertEqual(cm.exception.code, 'duplicate_sibling')
        test_element_count = MenuItem.objects.filter(
            url='/root2/test/', parent=root2).count()
        self.assertEqual(test_element_count, 1)

    def test_updates_successfully(self):
        """
        Subitem is successfully updated when there
        every parent has several child elements.
        """
        root1 = self._get_parent('/root1')
        root2 = self._get_parent('/root2')
        other_children = [
            {'url': '/root2/test/', 'name': 'Element 1-0', 'parent': root1},
            {'url': '/root2/test/', 'name': 'Element 2-0', 'parent': root2},
            {'url': '/root1/test1/', 'name': 'Element 1-1', 'parent': root1},
            {'url': '/root1/test2/', 'name': 'Element 1-2', 'parent': root1},
            {'url': '/root2/test2/', 'name': 'Element 2-2', 'parent': root2},
            {'url': '/root2/test3/', 'name': 'Element 2-3', 'parent': root2},
        ]
        for element in other_children:
            self._insert_item(element)
        test_child = MenuItem.objects.get(url='/root2/test/', parent=root2)
        new_name = 'Test success!'
        test_child.name = new_name
        test_child.save()
        test_success = MenuItem.objects.filter(name=new_name).exists()
        self.assertTrue(test_success)
