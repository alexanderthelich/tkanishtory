# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-22 16:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menus', '0004_auto_20170322_0459'),
    ]

    operations = [
        migrations.RunSQL(
            """CREATE AGGREGATE array_agg_mult (VARIADIC arr anyarray)  (
                SFUNC     = array_cat
              , STYPE     = anyarray
              , INITCOND  = '{}'
            ); """,
            reverse_sql="DROP AGGREGATE IF EXISTS array_agg_mult(anyarray);"
        )
    ]
