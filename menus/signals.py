from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models import MenuItem


@receiver(pre_save, sender=MenuItem, dispatch_uid='unique_among_siblings')
def unique_among_siblings(sender, **kwargs):
    """ Checking if menu item url is unique among sibling menu items. """
    instance = kwargs['instance']
    instance_exists = sender.objects.filter(
        parent_id=instance.parent_id, url=instance.url).exists()
    if instance_exists and instance._state.adding:
        error_msg = 'No more than one menu item with'\
            ' {url} attached to the same parent'
        raise ValidationError(
            error_msg,
            code='duplicate_sibling',
            params={'url': instance.url},
        )
