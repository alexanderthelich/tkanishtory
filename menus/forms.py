from itertools import groupby

from django.contrib.admin import TabularInline
from django.contrib.contenttypes.admin import GenericTabularInline
from django.contrib.contenttypes.forms import BaseGenericInlineFormSet
from django.forms.models import (
    BaseInlineFormSet, ModelChoiceField, ModelChoiceIterator)

from utils.forms import (
    AutoreplacingFormSetMixin, TreeModelChoiceField, TreeModelForm)

from .models import MenuItem


class MenuItemInlineFormSet(AutoreplacingFormSetMixin, BaseInlineFormSet):
    autoreplace_conditions = [
        ('menu', lambda x,y: False, 'menu', True),
    ]


class MenuItemInline(TabularInline):
    classes = ('collapse',)
    model = MenuItem
    extra = 1
    exclude = ('menu', 'object_id', 'content_type')
    formset = MenuItemInlineFormSet


class GenericMenuItemChoiceIterator(ModelChoiceIterator):
    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label)
        queryset = self.queryset.all()
        groups = groupby(queryset, lambda x: x.get_menu_display())
        choices = [(menu, [self.choice(item) for item in items])
                   for menu, items in groups]
        for choice in choices:
            yield choice


class GenericMenuItemChoiceField(TreeModelChoiceField):
    iterator = GenericMenuItemChoiceIterator


class GenericMenuItemInlineForm(TreeModelForm):
    def clean_menu(self):
        instance_menu = self.cleaned_data.get('menu')
        parent_pk = self['parent'].value()
        if parent_pk:
            parent_menu = self.instance.__class__.objects.get(pk=parent_pk).menu
        else:
            parent_menu = None
        if instance_menu != parent_menu:
            instance_menu = parent_menu if parent_menu else instance_menu
            self.instance.get_descendants().update(menu=instance_menu)
        return instance_menu

    class Meta(TreeModelForm.Meta):
        field_classes = {
            'parent': GenericMenuItemChoiceField,
        }


class GenericMenuItemInlineFormSet(
        AutoreplacingFormSetMixin, BaseGenericInlineFormSet):
    autoreplace_conditions = [
        ('name', lambda x,y: bool(x), '__str__', False),
        ('url', lambda x,y: bool(x) and x.endswith('/'), 'menu_link', False),
        ('visible', lambda x,y: bool(y.visible), False, False),
    ]


class GenericMenuItemInline(GenericTabularInline):
    classes = ('collapse',)
    extra = 1
    formset = GenericMenuItemInlineFormSet
    model = MenuItem
    form = GenericMenuItemInlineForm


class MenuItemAdminModelForm(TreeModelForm):
    def clean_url(self):
        url = self.cleaned_data.get('url')
        return url if url.endswith('/') else url + '/'
