from django.contrib import messages

from cloth_store.admin import site
from utils.admin import TreeAdmin, VisibleTreeListFilter

from .forms import MenuItemAdminModelForm, MenuItemInlineFormSet, MenuItemInline
from .models import MenuItem


class VisibleMenuItemListFilter(VisibleTreeListFilter):
    parameter_name = 'menu_item_visible'

    def queryset(self, request, queryset):
        queryset = super().queryset(request, queryset)
        return queryset.menu_ordered()


class MenuItemAdmin(TreeAdmin):
    form = MenuItemAdminModelForm
    search_fields = ['name']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.menu_ordered()

    def add_all_siblings_link(self, request, queryset):
        model = self.model
        item_name = 'Все'
        success_count = 0
        for parent in queryset:
            filters = {
                'parent_id': parent.pk,
                'url': parent.url,
                'name': item_name,
            }
            if not model.objects.filter(**filters).exists():
                success_count += 1
                for k, v in filters.items():
                    setattr(parent, k, v)
                parent.pk = None
                parent.sort = 1100
                parent.save()
        if success_count:
            message = 'Успешно создано {} пункта меню.'.format(success_count)
            self.message_user(request, message, messages.SUCCESS)
    add_all_siblings_link.short_description = 'Добавить ссылку "Все"'

    actions = ['move_tree', 'add_all_siblings_link']
    list_display = ('depth_padded_name', 'visible', 'sort',)
    list_filter = ('menu', VisibleMenuItemListFilter)
    inlines = [MenuItemInline]


site.register(MenuItem, MenuItemAdmin)
