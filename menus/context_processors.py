from collections import namedtuple

from django.db.models import Exists, OuterRef
from django.urls import reverse

from catalog.models import Sale, WareDiscount
from menus.models import MenuItem


def menu_tree(request):
    """
    For every menu in MenuItem.MENU constant creates context variable
    with menu slug as name. Variables are filled with all visible menu
    items of their respective menu forming tree-like structure as flat
    list. Menus without menu items remain in context as empty lists.
    """
    context = {x[0]:[] for x in MenuItem.MENU}
    all_items = MenuItem.hierarchy.visible_items().menu_ordered()
    anchor_url = reverse('catalog:discounts_non_sale')
    sale_root = reverse('catalog:discounts_all')
    sale_root_id = None
    for item in all_items:
        context[item.menu].append(item)
        if item.url == anchor_url:
            context[item.menu].extend(_get_sale_items(sale_root_id))
        elif item.url == sale_root:
            sale_root_id = sale_root_id or item.pk
    return context

def _get_sale_items(root_id):
    """
    Adding menu items for sales and discounts here instead of
    adding record to MenuItem's database table because sales
    are temporary in their nature and they have to disappear
    without user intervention as opposed to other menu items
    which have explicit visibilty flag changable by the user.
    """
    exists = Exists(WareDiscount.objects.filter(sale_id=OuterRef('pk')))
    sales = Sale.objects.annotate(active=exists).filter(active=True)
    menu_items = []
    for sale in sales:
        menu_item = MenuItem(name=sale.name, url=sale.get_absolute_url(),
                             pk='sale'+str(sale.pk), parent_id=root_id)
        menu_items.append(menu_item)
    return menu_items
