from django.apps import AppConfig
from django.db.models.signals import pre_save


class MenusConfig(AppConfig):
    name = 'menus'
    verbose_name = 'Меню'

    def ready(self):
        from .models import MenuItem
        from .signals import unique_among_siblings
        pre_save.connect(unique_among_siblings, sender=MenuItem,
                         dispatch_uid='unique_among_siblings')
